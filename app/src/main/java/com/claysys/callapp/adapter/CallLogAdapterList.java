package com.claysys.callapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.claysys.callapp.R;
import com.claysys.callapp.sqllite.helper.CallLogTb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class CallLogAdapterList extends RecyclerView.Adapter<CallLogAdapterList.ViewHolder> {
    private ArrayList<CallLogTb> callLogListAdapter;
    private Context context;
    CallLogAdapterList.OnClickCallLogAdapter onClickContactAdapter;


    public CallLogAdapterList(Context context, ArrayList<CallLogTb> items, CallLogAdapterList.OnClickCallLogAdapter onClickContactAdapter) {
        this.context = context;
        this.callLogListAdapter = items;
        this.onClickContactAdapter = onClickContactAdapter;


    }



    @Override
    public CallLogAdapterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recent_cell, parent, false);
        return new CallLogAdapterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CallLogAdapterList.ViewHolder holder, final int position) {
        holder.tvName.setText(callLogListAdapter.get(position).getName());


        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM");

        long timestamp = Long.parseLong(callLogListAdapter.get(position).getDate());
        timestamp = timestamp*1000;
        String dateString = formatter.format(new Date(timestamp));

        formatter = new SimpleDateFormat("hh:mm a");
        long timestampDate = Long.parseLong(callLogListAdapter.get(position).getDate());
        timestampDate = timestampDate*1000;

        Calendar c = Calendar.getInstance();
        Date todayFullDate = c.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM");
        String todayFullDateToCheck = df.format(todayFullDate);

        if (todayFullDateToCheck.equalsIgnoreCase(dateString)) {
            dateString = formatter.format(new Date(timestampDate));
            holder.tvDate.setText("Today");

        }else{
            c.add(Calendar.DATE, -1);
            Date ysturdayDate = c.getTime();
            String yesterdayAsString = df.format(ysturdayDate);
            if (yesterdayAsString.equalsIgnoreCase(dateString)) {
                holder.tvDate.setText("Yesterday");
            }else{
                holder.tvDate.setText(dateString);
            }
        }
        holder.tvTime.setText(formatter.format(new Date(timestampDate)));

        holder.tvphnumber.setText(callLogListAdapter.get(position).getNumberType()+"  "+callLogListAdapter.get(position).getPhNo());

        holder.llCallLogContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLogListAdapter.get(position).setDateDiaplay(holder.tvDate.getText().toString()+" "+holder.tvTime.getText().toString());
                onClickContactAdapter.onClickCallLog(callLogListAdapter.get(position));

            }
        });
        if (callLogListAdapter.get(position).getImgeUrl()!=null) {

            Glide.with(context)
                    .load(callLogListAdapter.get(position).getImgeUrl())
                    .fitCenter()
                    .placeholder(context.getResources().getDrawable(R.drawable.img_avatar))
                    .into(holder.imgProfileImage);
        }

    }

    @Override
    public int getItemCount() {
        return callLogListAdapter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName,tvTime, tvDate, tvphnumber;
        ImageView record_img, imgProfileImage;
        RelativeLayout llCallLogContainer;



        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_log);
            tvTime = itemView.findViewById(R.id.tv_time);
            record_img=itemView.findViewById(R.id.record_img);
            imgProfileImage=itemView.findViewById(R.id.img_profile_image_i);
            llCallLogContainer =itemView.findViewById(R.id.ll_call_log_container);
            tvDate =itemView.findViewById(R.id.tv_date);
            tvphnumber =itemView.findViewById(R.id.phnumber);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnClickCallLogAdapter {
        void onClickCallLog(CallLogTb callLogTb);

    }

}
