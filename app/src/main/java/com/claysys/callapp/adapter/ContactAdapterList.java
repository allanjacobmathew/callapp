package com.claysys.callapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.claysys.callapp.R;
import com.claysys.callapp.fragment.CallLogFragment;
import com.claysys.callapp.sqllite.helper.ContactTb;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ContactAdapterList extends RecyclerView.Adapter<ContactAdapterList.ViewHolder> {
    private ArrayList<ContactTb> contactListArrayList;
    private Context context;
    OnClickContactAdapter onClickContactAdapter;


    public ContactAdapterList(Context context, ArrayList<ContactTb> items, OnClickContactAdapter onClickContactAdapter) {
        this.context = context;
        this.contactListArrayList = items;
        this.onClickContactAdapter = onClickContactAdapter;


    }

    public ContactAdapterList(FragmentActivity activity, CallLogFragment callLogFragment) {

    }

    @Override
    public ContactAdapterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_cell, parent, false);
        return new ContactAdapterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactAdapterList.ViewHolder holder, final int position) {

           holder.tvName.setText(contactListArrayList.get(position).getFirstName() + " " + contactListArrayList.get(position).getLastName());
           holder.tvPhnNo.setText(contactListArrayList.get(position).getMobile());
           holder.tvPhnNo_work.setText(contactListArrayList.get(position).getAlternate());
           holder.tv_email.setText(contactListArrayList.get(position).getEmailID());
           if (contactListArrayList.get(position).getImageUrl() != null) {
//              holder.imgProfileImage.setImageURI(Uri.parse());

               Glide.with(context)
                       .load(contactListArrayList.get(position).getImageUrl())
                       .fitCenter()
                       .apply(new RequestOptions())
                       .placeholder(context.getResources().getDrawable(R.drawable.img_avatar))
                       .into(holder.imgProfileImage);
           }
           holder.llMobile.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   onClickContactAdapter.onClickContact(contactListArrayList.get(position), false);
               }
           });
           holder.llWork.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   onClickContactAdapter.onClickContact(contactListArrayList.get(position), true);
               }
           });
        holder.imGoToMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",contactListArrayList.get(position).getEmailID(), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(intent, "Choose an Email client :"));
//                onClickContactAdapter.onGoToMsgActivity(contactListArrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactListArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPhnNo,tvPhnNo_work,tv_email;
        CircleImageView imgProfileImage;
        ImageView imMobilCall,imWorkCall,imGoToMessage;
        LinearLayout llWork, llMobile;


        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPhnNo = itemView.findViewById(R.id.tv_phn_no);
            tvPhnNo_work = itemView.findViewById(R.id.tv_phn_no_work);
            tv_email = itemView.findViewById(R.id.tv_email);
            imgProfileImage = itemView.findViewById(R.id.img_profile_image_i);
            imMobilCall = itemView.findViewById(R.id.im_mobile_call);
            imWorkCall = itemView.findViewById(R.id.im_work_call);
            imGoToMessage = itemView.findViewById(R.id.imGoToMessage);
            llWork = itemView.findViewById(R.id.ll_work);
            llMobile = itemView.findViewById(R.id.ll_mobile);

        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnClickContactAdapter {
        void onClickContact(ContactTb contactTb,boolean isWork);
        void onGoToMsgActivity(ContactTb contactTb);

    }

    public void filter(String text, List<ContactTb> contactList) {
        int numberFlag = 0;
        //new array list that will hold the filtered data
        ArrayList<ContactTb> filterdNames = new ArrayList<>();
//        try
//        {
//            Integer.parseInt(text);
//            numberFlag=1;
//        } catch (NumberFormatException e) {
//            numberFlag =0;
//        }

//        if(numberFlag ==0) {
            //looping through existing elements
            for (ContactTb s : contactList) {
                //if the existing elements contains the search input
                if (s.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                    ContactTb contactDetails = new ContactTb();
                    contactDetails = s;
                    //adding the element to filtered list
                    filterdNames.add(contactDetails);

                }
            }
            for (ContactTb s : contactList) {
                //if the existing elements contains the search input
                if (s.getLastName().toLowerCase().contains(text.toLowerCase())) {
                    ContactTb contactDetails = new ContactTb();
                    contactDetails = s;
                    //adding the element to filtered list
                    filterdNames.add(contactDetails);

                }
            }


        this.contactListArrayList = filterdNames;
        notifyDataSetChanged();
        //calling a method of the adapter class and passing the filtered list

    }
    public void setAllDataList(ArrayList<ContactTb> contactDetailsList ){
        this.contactListArrayList = contactDetailsList;
        notifyDataSetChanged();
    }

}
