package com.claysys.callapp.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.App;
import com.claysys.callapp.ChatActivity;
import com.claysys.callapp.MainActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.dialogs.SelectContactSiimpleDialog;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.models.GroupChatMsgs;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.AppConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChatRoomAdapterList extends RecyclerView.Adapter<ChatRoomAdapterList.ViewHolder> {
    private ArrayList<GroupChatMsgs> groupChatMsgList;
    private Context context;
    SharedPreferredManager sharedPreferredManager;
    ArrayList<ContactTb> contact_tbArrayList;
    SimpleContactListAdapter.OnClickContactToTagtUser onClickContactToTagtUser;
    String tagedUserName;
    int startFlag = 0;

    public ChatRoomAdapterList(Context context, ArrayList<GroupChatMsgs> groupChatMsgList, SimpleContactListAdapter.OnClickContactToTagtUser onClickContactToTagtUser) {
        this.context = context;
        this.groupChatMsgList = groupChatMsgList;
        this.onClickContactToTagtUser = onClickContactToTagtUser;
    }

    @Override
    public ChatRoomAdapterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_chatroom_single, parent, false);
        sharedPreferredManager = new SharedPreferredManager(context);
        return new ChatRoomAdapterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatRoomAdapterList.ViewHolder holder, final int position) {
        startFlag = 0;
        if (groupChatMsgList.get(position).isEnd()) {
            if (position == 0) {
                holder.btnTagAUser.setVisibility(View.VISIBLE);
                holder.llSingleMessageContainer.setVisibility(View.GONE);
                holder.tvTagUsername.setVisibility(View.GONE);

            }

            if (groupChatMsgList.get(position).isStart()) {
                if (groupChatMsgList.get(position).getTaggedUserId() != null) {
                    filter(groupChatMsgList.get(position).getTaggedUserId(), contact_tbArrayList);
                    holder.tvTagUsername.setVisibility(View.VISIBLE);
                    holder.tvTagUsername.setText(tagedUserName + " is tagged");
                    startFlag = 1;
                }
                if (groupChatMsgList.get(position).getUserToBeTagged() != null) {
                    holder.btnTagAUser.setVisibility(View.VISIBLE);
                    filter(groupChatMsgList.get(position).getUserToBeTagged(), contact_tbArrayList);
                    holder.btnTagAUser.setText("Following messages will be tagged aganist " + tagedUserName);
                } else {
                    holder.btnTagAUser.setVisibility(View.VISIBLE);
                    holder.btnTagAUser.setText(AppConstants.TagAMember);
                }
            } else if (groupChatMsgList.get(position).getUserToBeTagged() != null) {
                holder.btnTagAUser.setVisibility(View.VISIBLE);
                filter(groupChatMsgList.get(position).getUserToBeTagged(), contact_tbArrayList);
                holder.btnTagAUser.setText("Following messages will be tagged aganist " + tagedUserName);
            } else {
                holder.tvTagUsername.setVisibility(View.GONE);
                holder.btnTagAUser.setVisibility(View.VISIBLE);
                holder.btnTagAUser.setText(AppConstants.TagAMember);

            }
            if (groupChatMsgList.size() >= position + 2) {
                if (groupChatMsgList.get(position + 1) != null) {
                    if (groupChatMsgList.get(position + 1).isStart()) {
                        holder.btnTagAUser.setVisibility(View.GONE);
                        if (startFlag != 1) {
                            holder.tvTagUsername.setVisibility(View.GONE);
                        }
                    }
                }
            }

        } else if (groupChatMsgList.get(position).isStart()) {
            holder.tvTagUsername.setVisibility(View.VISIBLE);
            filter(groupChatMsgList.get(position).getTaggedUserId(), contact_tbArrayList);
            holder.tvTagUsername.setText(tagedUserName + " is tagged");
            holder.llSingleMessageContainer.setVisibility(View.GONE);
            holder.btnTagAUser.setVisibility(View.GONE);
        } else {
            holder.tvTagUsername.setVisibility(View.GONE);
            holder.btnTagAUser.setVisibility(View.GONE);
            holder.llSingleMessageContainer.setVisibility(View.GONE);
        }


//        if (groupChatMsgList.get(position).isEnd()) {
//            if (groupChatMsgList.size() >= position + 2) {
//                if (groupChatMsgList.get(position + 1) != null) {
//                    if (groupChatMsgList.get(position + 1).isStart()) {
//                        holder.btnTagAUser.setVisibility(View.GONE);
//                        holder.tvTagUsername.setVisibility(View.GONE);
//                    } else {
//                        holder.btnTagAUser.setVisibility(View.VISIBLE);
//                        holder.tvTagUsername.setVisibility(View.GONE);
//                        holder.llSingleMessageContainer.setVisibility(View.GONE);
//                        if (groupChatMsgList.get(position).isStart()) {
//                            filter(groupChatMsgList.get(position).getTaggedUserId(), contact_tbArrayList);
//                            holder.btnTagAUser.setText(tagedUserName + " is tagged");
//                        }
//                    }
//                }
//            } else {
//                holder.btnTagAUser.setVisibility(View.VISIBLE);
//                holder.btnEndStartTaAMember.setVisibility(View.VISIBLE);
//                holder.tvTagUsername.setVisibility(View.GONE);
//                holder.llSingleMessageContainer.setVisibility(View.GONE);
//                if (groupChatMsgList.get(position).isStart()) {
//                    filter(groupChatMsgList.get(position).getUserToBeTagged(), contact_tbArrayList);
//                    holder.btnTagAUser.setText(tagedUserName + " is tagged");
//                }
//            }
//
//
//        }
//        else {
//            holder.btnTagAUser.setVisibility(View.GONE);
//
//            if (groupChatMsgList.get(position).isStart()) {
//                holder.tvTagUsername.setVisibility(View.VISIBLE);
//                filter(groupChatMsgList.get(position).getTaggedUserId(), contact_tbArrayList);
//                holder.tvTagUsername.setText(tagedUserName + " is tagged");
//                holder.llSingleMessageContainer.setVisibility(View.GONE);
//            } else {
//                holder.tvTagUsername.setVisibility(View.GONE);
//                holder.llSingleMessageContainer.setVisibility(View.GONE);
//            }
//        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (groupChatMsgList.get(position).getMsgSenderId() != null) {
            holder.llSingleMessageContainer.setVisibility(View.VISIBLE);

            if (groupChatMsgList.get(position).getMsgSenderId().equals(sharedPreferredManager.getUserFirebaseId())) {
                holder.tvChaterName.setVisibility(View.GONE);
                params.gravity = Gravity.RIGHT;
                holder.llChatContainer.setBackground(context.getResources().getDrawable(R.drawable.rightbubble_ninepatch));
                holder.tvMsg.setTextColor(context.getResources().getColor(R.color.white));
            } else {
                params.gravity = Gravity.LEFT;
                holder.tvMsg.setTextColor(context.getResources().getColor(R.color.secondryTextColor));
                holder.llChatContainer.setBackground(context.getResources().getDrawable(R.drawable.leftbubble_nine_patch));

            }
            holder.llSingleMessageContainer.setLayoutParams(params);
            Long timestamp;
            String dateString;


            if (groupChatMsgList.get(position).getCreatedDateTimeStamp() == null) {
                timestamp = System.currentTimeMillis();
                //  dateString = timestamp.toString();
            } else {
                timestamp = Long.valueOf(groupChatMsgList.get(position).getCreatedDateTimeStamp()) * 1000;
            }


            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ");
            dateString = formatter.format(new Date(timestamp));

            Calendar c = Calendar.getInstance();
            Date todayFullDate = c.getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd MMM ");
            String todayFullDateToCheck = df.format(todayFullDate);
            formatter = new SimpleDateFormat("hh:mm a");

            if (todayFullDateToCheck.equalsIgnoreCase(dateString)) {

                dateString = "Today " + formatter.format(new Date(timestamp));

            }else {
                c.add(Calendar.DATE, -1);
                Date ysturdayDate = c.getTime();
                String yesterdayAsString = df.format(ysturdayDate);
                if (yesterdayAsString.equalsIgnoreCase(dateString)) {
                    dateString = "Yesterday " + formatter.format(new Date(timestamp));

                }else{
                    formatter = new SimpleDateFormat("dd MMM, hh:mm a");
                    dateString = formatter.format(new Date(timestamp));
                }
            }
            holder.msg_time.setText(dateString);
            holder.tvMsg.setText(groupChatMsgList.get(position).getMsg());
        }
        holder.btnTagAUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SelectContactSiimpleDialog().openUpDialogToAdd(context, position, contact_tbArrayList, onClickContactToTagtUser);
            }
        });
        if (position == groupChatMsgList.size() - 1) {
            if (holder.btnTagAUser.getVisibility() == View.VISIBLE) {
                if (holder.btnTagAUser.getText().toString().equals(AppConstants.TagAMember)) {
                    ((ChatActivity) context).setVisibleOfSendContainer(false);
                } else {
                    ((ChatActivity) context).setVisibleOfSendContainer(true);
                }
            } else {
                ((ChatActivity) context).setVisibleOfSendContainer(true);
            }
        }

    }

    @Override
    public int getItemCount() {
        return groupChatMsgList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout llSingleMessageContainer;
        protected TextView tvChaterName;
        protected TextView tvMsg, msg_time;
        protected TextView tvTagUsername;
        Button btnTagAUser, btnEndStartTaAMember;
        protected LinearLayout llChatContainer, llQuestionContainer;


        public ViewHolder(View itemView) {
            super(itemView);
            llSingleMessageContainer = itemView.findViewById(R.id.ll_single_Message_Container);
            llChatContainer = itemView.findViewById(R.id.ll_chat_container);
            tvChaterName = itemView.findViewById(R.id.tv_chater_name);
            tvMsg = itemView.findViewById(R.id.tv_msg);
            msg_time = itemView.findViewById(R.id.msg_time);
            btnTagAUser = itemView.findViewById(R.id.btn_tag_a_user);
            tvTagUsername = itemView.findViewById(R.id.tv_tag_username);
//            btnEndStartTaAMember = itemView.findViewById(R.id.btn_end_start_tag_a_member);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setContactList(ArrayList<ContactTb> contactTbArrayList) {
        contact_tbArrayList = contactTbArrayList;
    }

    public void filter(String id, List<ContactTb> contactList) {
        //new array list that will hold the filtered data
        if (contactList != null) {
            ArrayList<ContactTb> filterdNames = new ArrayList<>();

            //looping through existing elements
            for (ContactTb s : contactList) {
                //if the existing elements contains the search input
                if (s.getId().contains(id)) {
                    ContactTb contactDetails = new ContactTb();
                    contactDetails = s;
                    //adding the element to filtered list
                    filterdNames.add(contactDetails);

                }
            }
            if (filterdNames.size() > 0)
                this.tagedUserName = filterdNames.get(0).getFirstName() + " " + filterdNames.get(0).getLastName();
        }
    }

}
