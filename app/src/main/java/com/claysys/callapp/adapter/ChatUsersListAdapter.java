package com.claysys.callapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.claysys.callapp.R;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class ChatUsersListAdapter extends RecyclerView.Adapter<ChatUsersListAdapter.ViewHolder> {
    private ArrayList<ContactTb> callLogListAdapter;
    private Context context;
    OnClickChatListUser onClickChatListUser;


    public ChatUsersListAdapter(Context context, ArrayList<ContactTb> items, OnClickChatListUser onClickChatListUser) {
        this.context = context;
        this.callLogListAdapter = items;
        this.onClickChatListUser = onClickChatListUser;

    }


    @Override
    public ChatUsersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_chat_list_single_row, parent, false);
        return new ChatUsersListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatUsersListAdapter.ViewHolder holder, final int position) {
        if (callLogListAdapter.get(position).getIsUsersList()) {
            holder.tvName.setText(callLogListAdapter.get(position).getName());
            holder.tvTime.setVisibility(View.GONE);
            holder.tvDate.setVisibility(View.GONE);
            holder.llLastmsg.setVisibility(View.GONE);
            holder.rightArow.setVisibility(View.GONE);
        }else {
            if (callLogListAdapter.get(position).getUserDetails()!=null){
                if(callLogListAdapter.get(position).getMsg_timestamp()!=null) {
                    Long timestamp;
                    if(callLogListAdapter.get(position).getMsg_timestamp().length()>10){
                         timestamp = Long.valueOf(callLogListAdapter.get(position).getMsg_timestamp());
                    }else{
                         timestamp = Long.valueOf(callLogListAdapter.get(position).getMsg_timestamp()) * 1000;
                    }

                    SimpleDateFormat formatter =  new SimpleDateFormat("hh:mm a");
                    String time = formatter.format(new Date(timestamp));
                    holder.tvTime.setText(time);
                    Calendar c = Calendar.getInstance();

                    Date todayFullDate = c.getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd MMM");
                    String todayFullDateToCheck = df.format(todayFullDate);
                    String dateString = df.format(new Date(timestamp));

                    if (todayFullDateToCheck.equalsIgnoreCase(dateString)) {
                        formatter = new SimpleDateFormat("hh:mm a");
                        dateString = "Today ";

                    }else {
                        c.add(Calendar.DATE, -1);
                        Date ysturdayDate = c.getTime();
                        String yesterdayAsString = df.format(ysturdayDate);
                        if (yesterdayAsString.equalsIgnoreCase(dateString)) {
                            dateString = "Yesterday ";

                        }
                    }
                    holder.tvDate.setText(dateString);
                }
                holder.tvName.setText(callLogListAdapter.get(position).getUserDetails().getName());
                holder.last_msg.setText(callLogListAdapter.get(position).getLast_msg());
            }

        }

        holder.chatlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickChatListUser.onClickUserList(callLogListAdapter.get(position),callLogListAdapter.get(position).getIsUsersList());

            }
        });

    }

    @Override
    public int getItemCount() {
        return callLogListAdapter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvTime,last_msg, tvDate;
        ImageView imgProfileImage, rightArow;
        RelativeLayout chatlist;
        LinearLayout llLastmsg;


        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_log);

            tvTime = itemView.findViewById(R.id.tv_time);
            last_msg = itemView.findViewById(R.id.last_msg);
            imgProfileImage = itemView.findViewById(R.id.img_profile_image_i);
            chatlist = itemView.findViewById(R.id.chat_list);
            tvDate = itemView.findViewById(R.id.tv_date);
            llLastmsg = itemView.findViewById(R.id.ll_lastmsg);
            rightArow = itemView.findViewById(R.id.rightarow);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnClickChatListUser {
        void onClickUserList(ContactTb contactTb,boolean isUserList);

    }

}
