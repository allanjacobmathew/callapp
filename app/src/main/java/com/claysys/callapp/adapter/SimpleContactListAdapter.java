package com.claysys.callapp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.R;
import com.claysys.callapp.sqllite.helper.ContactTb;

import java.util.ArrayList;
import java.util.List;


public class SimpleContactListAdapter extends RecyclerView.Adapter<SimpleContactListAdapter.ViewHolder> {
    private ArrayList<ContactTb> callLogListAdapter;
    private Context context;
    OnClickContactToTagtUser onClickContactToTagtUser;
    int chatRoomMsgPosition;
    Dialog mDialogAddStatus;


    public SimpleContactListAdapter(Context context, int position, Dialog mDialogAddStatus, ArrayList<ContactTb> items, OnClickContactToTagtUser onClickContactToTagtUser) {
        this.context = context;
        this.callLogListAdapter = items;
        this.onClickContactToTagtUser = onClickContactToTagtUser;
        chatRoomMsgPosition =position;
        this.mDialogAddStatus = mDialogAddStatus;
    }


    @Override
    public SimpleContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_simple_contact_list, parent, false);
        return new SimpleContactListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleContactListAdapter.ViewHolder holder, final int position) {
        holder.tvName.setText(callLogListAdapter.get(position).getFirstName()+" "+callLogListAdapter.get(position).getLastName());
        holder.simpleTaggedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogAddStatus.dismiss();
                onClickContactToTagtUser.onClickContactToTag(callLogListAdapter.get(position),chatRoomMsgPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(callLogListAdapter!=null) {
            return callLogListAdapter.size();
        }else
            return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView imgProfileImage;
        LinearLayout simpleTaggedList;


        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_log);
            simpleTaggedList = itemView.findViewById(R.id.simple_tagged_list);
            imgProfileImage = itemView.findViewById(R.id.img_profile_image_i);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnClickContactToTagtUser {
        void onClickContactToTag(ContactTb contactTb,int position );

    }
    public void filter(String text, List<ContactTb> contactList) {
        int numberFlag = 0;
        //new array list that will hold the filtered data
        ArrayList<ContactTb> filterdNames = new ArrayList<>();

        for (ContactTb s : contactList) {
            //if the existing elements contains the search input
            if (s.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                ContactTb contactDetails = new ContactTb();
                contactDetails = s;
                //adding the element to filtered list
                filterdNames.add(contactDetails);

            }
        }
        for (ContactTb s : contactList) {
            //if the existing elements contains the search input
            if (s.getLastName().toLowerCase().contains(text.toLowerCase())) {
                ContactTb contactDetails = new ContactTb();
                contactDetails = s;
                //adding the element to filtered list
                filterdNames.add(contactDetails);

            }
        }


        this.callLogListAdapter = filterdNames;
        notifyDataSetChanged();

    }
    public void setAllDataList(ArrayList<ContactTb> contactDetailsList ){
        this.callLogListAdapter = contactDetailsList;
        notifyDataSetChanged();
    }

}
