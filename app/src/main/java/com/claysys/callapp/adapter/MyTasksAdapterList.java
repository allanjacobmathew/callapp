package com.claysys.callapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.R;
import com.claysys.callapp.models.MyTaskList;

import java.util.ArrayList;


public class MyTasksAdapterList extends RecyclerView.Adapter<MyTasksAdapterList.ViewHolder> {
    private ArrayList<MyTaskList> myTaskLists;
    private Context context;
    MyTasksAdapterList.OnClickMyTaskListInterface OnClickMyTaskListInterface;


    public MyTasksAdapterList(Context context, ArrayList<MyTaskList> items, OnClickMyTaskListInterface OnClickMyTaskListInterface) {
        this.context = context;
        this.myTaskLists = items;
        this.OnClickMyTaskListInterface = OnClickMyTaskListInterface;


    }



    @Override
    public MyTasksAdapterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_my_tasks_list_row, parent, false);
        return new MyTasksAdapterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyTasksAdapterList.ViewHolder holder, final int position) {
        holder.tvName.setText(myTaskLists.get(position).getTaskName());
        holder.taskDescriptn.setText(myTaskLists.get(position).getTaskDescription());
        holder.tvTaskProgrs.setText(myTaskLists.get(position).getTaskProgress());
        holder.llMytaskContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickMyTaskListInterface.onClickMytaskList(myTaskLists.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return myTaskLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName,tvTaskProgrs,taskDescriptn;
        ImageView  imgProfileImage;
        LinearLayout llMytaskContainer;



        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_log);
            llMytaskContainer = itemView.findViewById(R.id.ll_mytask_container_layout);
            tvTaskProgrs = itemView.findViewById(R.id.tv_task_progrs);
            taskDescriptn=itemView.findViewById(R.id.task_descriptn);
            imgProfileImage=itemView.findViewById(R.id.img_profile_image);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnClickMyTaskListInterface {
        void onClickMytaskList(MyTaskList MyTaskList);

    }

}
