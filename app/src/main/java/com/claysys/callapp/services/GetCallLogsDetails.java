package com.claysys.callapp.services;

import android.Manifest;
import android.app.IntentService;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.CallLog;
import android.util.Log;

import androidx.annotation.Nullable;

import com.claysys.callapp.App;
import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.SaveCallLogReq;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.AppConstants;

import java.util.Date;

public class GetCallLogsDetails extends IntentService implements IResponseView {
    CallLogTb callLog_tb;

    public GetCallLogsDetails() {
        super("GetCallLogsDetails");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        StringBuffer sb = new StringBuffer();
        if (checkSelfPermission(Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        Log.e("s inside12311", "111111 ");
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
//            if (phNumber.length() > 10) {
//                phNumber = phNumber.substring(phNumber.length() - 10);
//            }// mobile number
            String callType = managedCursor.getString(type); // call type
            String callDate = managedCursor.getString(date); // call date
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "Outgoing";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "Incoming";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "Misssed";
                    break;
            }

            ContactTb contactTb;
             contactTb = App.getInstance().getDB().scheduleDao().getContactDetailsByNumber(phNumber);
            if (contactTb != null) {

                callLog_tb = new CallLogTb();
                callLog_tb.setUserid(String.valueOf(contactTb.getId()));
                callLog_tb.setLog(callType);
                callLog_tb.setTime(callDuration);
                callLog_tb.setDate(callDate);
                callLog_tb.setLog(dir);
                saveCallLogApi(callLog_tb, contactTb);

                long id = App.getInstance().getDB().scheduleDao().insertCalLLogDetails(callLog_tb);

            }else{
                contactTb = App.getInstance().getDB().scheduleDao().getContactDetailsByMobileNumber(phNumber);
                if (contactTb != null) {

                    callLog_tb = new CallLogTb();
                    callLog_tb.setUserid(String.valueOf(contactTb.getId()));
                    callLog_tb.setLog(callType);
                    callLog_tb.setTime(callDuration);
                    callLog_tb.setDate(callDate);
                    callLog_tb.setLog(dir);
                    saveCallLogApi(callLog_tb, contactTb);

                    long id = App.getInstance().getDB().scheduleDao().insertCalLLogDetails(callLog_tb);

                }
            }

            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        Log.e("Agil value --- ", sb.toString());


    }

    public void saveCallLogApi(CallLogTb callLog_tb, ContactTb contactTb) {
//        SaveCallLogReq saveCallLogReq = new SaveCallLogReq();
//        saveCallLogReq.setContactId(Integer.parseInt(callLog_tb.getUserid()));
//        saveCallLogReq.setNumber(callLog_tb.getPhNo());
//        saveCallLogReq.setEmailID(contactTb.getEmailID());
//        saveCallLogReq.setTime(Long.parseLong(callLog_tb.getDate()));
//        saveCallLogReq.setLogType(AppConstants.Phone);
//        saveCallLogReq.setGuid(contactTb.getGuid());
//        ApiHelper.getInstance().saveCallLogApi(getApplicationContext(), this, saveCallLogReq);
    }

    @Override
    public void onSaveCallLogsSucess(SaveCallLogResp saveCallLogResp) {
//        if (saveCallLogResp.getStatus().equals(AppConstants.Success)) {
//            callLog_tb.setLogId(saveCallLogResp.getId());
//            long id = App.getInstance().getDB().scheduleDao().insertCalLLogDetails(callLog_tb);
//        }
    }

    @Override
    public void onSaveCallLogsFailure() {

    }
}
