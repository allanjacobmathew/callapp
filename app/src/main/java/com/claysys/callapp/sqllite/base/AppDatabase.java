package com.claysys.callapp.sqllite.base;



import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.claysys.callapp.models.GroupChatMsgs;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.sqllite.dao.ScheduleDao;
import com.claysys.callapp.sqllite.helper.EmployeeTb;

@Database(entities = {CallLogTb.class, ContactTb.class, EmployeeTb.class},version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ScheduleDao scheduleDao();
    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
