package com.claysys.callapp.sqllite.dao;



import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.claysys.callapp.models.GroupChatMsgs;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.sqllite.helper.EmployeeTb;


import java.util.List;

import static androidx.room.OnConflictStrategy.IGNORE;

@Dao
public interface ScheduleDao {
    @Insert(onConflict = IGNORE)
    long insertContactDetails(ContactTb contact_tb);

    @Query("SELECT * FROM ContactTb order by firstName")
    public LiveData<List<ContactTb>> getAllContactTb();

    @Insert(onConflict = IGNORE)
    long insertEmployeeDetails(EmployeeTb employeeTb);

    @Query("SELECT * FROM ContactTb where  alternate = :number")
    ContactTb getContactDetailsByNumber(String number);

    @Query("SELECT * FROM ContactTb where  mobile = :number")
    ContactTb getContactDetailsByMobileNumber(String number);

    @Query("SELECT * FROM ContactTb where  id = :id")
    ContactTb getContactById(int id);

    @Insert(onConflict = IGNORE)
    long insertCalLLogDetails(CallLogTb callLogTb);

//    @Insert(onConflict = IGNORE)
//    long insertGroupChats(GroupChatMsgs groupChatMsgs);

    @Query("SELECT * FROM CallLogTb " +
            "where CallLogTb.id =:id ")
    CallLogTb getMsg(int id);


    @Query("UPDATE CallLogTb SET msg= :msg WHERE id = :id")
    int update_msg(String msg, String id );

    @Query("UPDATE CallLogTb SET audioFileUrl= :audioUrl WHERE id = :id")
    int updateAudioUrl(String audioUrl, int id );

    @Query("UPDATE CallLogTb SET amrFileUrl= :amrFile WHERE id = :id")
    int updateAmrAudioUrl(String amrFile, int id );

    @Query("UPDATE CallLogTb SET fileUrl= :fileUrl WHERE id = :id")
    int updateServerAudioFileUrl(String fileUrl, int id );

    @Query("SELECT * FROM CallLogTb order by date desc")
    public LiveData<List<CallLogTb>> getAllCallLogs();

    @Query("UPDATE CallLogTb SET  msg= :msg,audioFileUrl = :audioFile, amrFileUrl= :amrFileUrl   WHERE id = :id")
    void deleteCallLog(String msg,String audioFile, String amrFileUrl,int id);



}
