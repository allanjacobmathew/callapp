package com.claysys.callapp.sqllite.helper;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(indices = {@Index(value = "date", unique = true)})
public class CallLogTb implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    int id;

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }
    @SerializedName("logId")
    @Expose
    private int logId;

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("time")
    private String time;
    @SerializedName("msg")
    private String msg;

    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    public void setAudioFileUrl(String audioFileUrl) {
        this.audioFileUrl = audioFileUrl;
    }

    @SerializedName("log")
    private String log;
    @SerializedName("date")
    private String date;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @SerializedName("fileUrl")
    private String fileUrl;

    private String audioFileUrl;
    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public String getAmrFileUrl() {
        return amrFileUrl;
    }

    public void setAmrFileUrl(String amrFileUrl) {
        this.amrFileUrl = amrFileUrl;
    }

    @SerializedName("amrFileUrl")
    private String amrFileUrl;

    @SerializedName("numberType")
    private String numberType;

    @Ignore
    private String name;

    public String getDateDiaplay() {
        return dateDiaplay;
    }

    public void setDateDiaplay(String dateDiaplay) {
        this.dateDiaplay = dateDiaplay;
    }

    @Ignore
    private String dateDiaplay;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    private String phNo;

    @Ignore
    private String guid;


    public String getImgeUrl() {
        return imgeUrl;
    }

    public void setImgeUrl(String imgeUrl) {
        this.imgeUrl = imgeUrl;
    }

    @Ignore
    private String imgeUrl;


    @SerializedName("id")
    public String getUserid() {
        return userid;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    @SerializedName("id")
    public void setUserid(String userid) {
        this.userid = userid;
    }

    @SerializedName("time")
    public String getTime() {
        return time;
    }

    @SerializedName("time")
    public void setTime(String time) {
        this.time = time;
    }

    @SerializedName("msg")
    public String getMsg() {
        return msg;
    }

    @SerializedName("msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.logId);
        dest.writeString(this.userid);
        dest.writeString(this.time);
        dest.writeString(this.msg);
        dest.writeString(this.log);
        dest.writeString(this.date);
        dest.writeString(this.fileUrl);
        dest.writeString(this.audioFileUrl);
        dest.writeString(this.amrFileUrl);
        dest.writeString(this.numberType);
        dest.writeString(this.name);
        dest.writeString(this.dateDiaplay);
        dest.writeString(this.phNo);
        dest.writeString(this.guid);
        dest.writeString(this.imgeUrl);
    }

    public CallLogTb() {
    }

    protected CallLogTb(Parcel in) {
        this.id = in.readInt();
        this.logId = in.readInt();
        this.userid = in.readString();
        this.time = in.readString();
        this.msg = in.readString();
        this.log = in.readString();
        this.date = in.readString();
        this.fileUrl = in.readString();
        this.audioFileUrl = in.readString();
        this.amrFileUrl = in.readString();
        this.numberType = in.readString();
        this.name = in.readString();
        this.dateDiaplay = in.readString();
        this.phNo = in.readString();
        this.guid = in.readString();
        this.imgeUrl = in.readString();
    }

    public static final Parcelable.Creator<CallLogTb> CREATOR = new Parcelable.Creator<CallLogTb>() {
        @Override
        public CallLogTb createFromParcel(Parcel source) {
            return new CallLogTb(source);
        }

        @Override
        public CallLogTb[] newArray(int size) {
            return new CallLogTb[size];
        }
    };
}
