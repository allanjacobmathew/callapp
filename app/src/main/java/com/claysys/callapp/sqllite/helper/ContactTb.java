package com.claysys.callapp.sqllite.helper;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(indices = {@Index(value = "id", unique = true)})
public class ContactTb implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int aId;

    public int getaId() {
        return aId;
    }

    public void setaId(int aId) {
        this.aId = aId;
    }

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("firstName")
    public String getName() {
        return firstName;
    }

    @SerializedName("firstName")
    public void setName(String name) {
        this.firstName = name;
    }

    @SerializedName("mobile")
    public String getMobile() {
        return mobile;
    }

    @SerializedName("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDocment_id() {
        return firebaseId;
    }

    public void setDocment_id(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    @Ignore
      String firebaseId;

    public String getMemberid1() {
        return memberid1;
    }

    public void setMemberid1(String memberid1) {
        this.memberid1 = memberid1;
    }

    public String getMemberid2() {
        return memberid2;
    }

    public void setMemberid2(String memberid2) {
        this.memberid2 = memberid2;
    }

    @Ignore
    String memberid1;
    String memberid2;

    public String getLast_msg() {
        return last_msg;
    }

    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }

    @Ignore
    String last_msg;

    public String getMsg_timestamp() {
        return msg_timestamp;
    }

    public void setMsg_timestamp(String msg_timestamp) {
        this.msg_timestamp = msg_timestamp;
    }

    @Ignore
    String msg_timestamp;

    public ContactTb getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(ContactTb userDetails) {
        this.userDetails = userDetails;
    }

    @Ignore
    ContactTb userDetails;



    public boolean getIsUsersList() {
        return isUsersList;
    }

    public void setIsUsersList(boolean isUsersList) {
        this.isUsersList = isUsersList;
    }

    @Ignore
    boolean isUsersList;

    String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getAlternate() {
        return alternate;
    }

    public void setAlternate(String alternate) {
        this.alternate = alternate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    String id;
    String emailID;
    String alternate;
    String imageUrl;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    String guid;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.aId);
        dest.writeString(this.firstName);
        dest.writeString(this.mobile);
        dest.writeString(this.firebaseId);
        dest.writeString(this.memberid1);
        dest.writeString(this.memberid2);
        dest.writeString(this.last_msg);
        dest.writeString(this.msg_timestamp);
        dest.writeParcelable(this.userDetails, flags);
        dest.writeByte(this.isUsersList ? (byte) 1 : (byte) 0);
        dest.writeString(this.lastName);
        dest.writeString(this.id);
        dest.writeString(this.emailID);
        dest.writeString(this.alternate);
        dest.writeString(this.imageUrl);
        dest.writeString(this.guid);
    }

    public ContactTb() {
    }

    protected ContactTb(Parcel in) {
        this.aId = in.readInt();
        this.firstName = in.readString();
        this.mobile = in.readString();
        this.firebaseId = in.readString();
        this.memberid1 = in.readString();
        this.memberid2 = in.readString();
        this.last_msg = in.readString();
        this.msg_timestamp = in.readString();
        this.userDetails = in.readParcelable(ContactTb.class.getClassLoader());
        this.isUsersList = in.readByte() != 0;
        this.lastName = in.readString();
        this.id = in.readString();
        this.emailID = in.readString();
        this.alternate = in.readString();
        this.imageUrl = in.readString();
        this.guid = in.readString();
    }

    public static final Parcelable.Creator<ContactTb> CREATOR = new Parcelable.Creator<ContactTb>() {
        @Override
        public ContactTb createFromParcel(Parcel source) {
            return new ContactTb(source);
        }

        @Override
        public ContactTb[] newArray(int size) {
            return new ContactTb[size];
        }
    };
}

