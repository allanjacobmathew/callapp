package com.claysys.callapp;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.claysys.callapp.sqllite.helper.CallLogTb;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecordActivity extends AppCompatActivity implements
        RecognitionListener {

    private static final int REQUEST_RECORD_PERMISSION = 100;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_call_name)
    TextView tvCallName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_call_phone)
    TextView tvCallPhone;
    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.tv_call_duration)
    TextView tvCallDuration;
    @BindView(R.id.tv_Date)
    TextView tvDate;
    @BindView(R.id.tv_call_date)
    TextView tvCallDate;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;

    private ProgressBar progressBar;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";

    @BindView(R.id.speech_tv)
    TextView speech_tv;
    @BindView(R.id.record_voice)
    ImageView recordvoice_start;
    @BindView(R.id.record_stop)
    ImageView recordvoice_stop;

    CallLogTb callLog_tb;
//    SaveAudioRecorder saveAudioRecorder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        callLog_tb = intent.getParcelableExtra("callLogTb");


        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        if (callLog_tb != null) {
            if (callLog_tb.getMsg() != null) {
                speech_tv.setText(callLog_tb.getMsg());
            }
            tvCallName.setText(callLog_tb.getName());
            tvCallPhone.setText(callLog_tb.getPhNo());
            Date callDayTime = new Date(Long.valueOf(callLog_tb.getDate()));
            SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
            String date = format.format(callDayTime);
            tvCallDate.setText(date);
            tvCallDuration.setText(callLog_tb.getTime());
        }


        progressBar.setVisibility(View.INVISIBLE);
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        Log.i(LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this));
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        recognizerIntent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        recognizerIntent.putExtra("android.speech.extra.GET_AUDIO", true);
        startActivityForResult(recognizerIntent, 10);
        recordvoice_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordvoice_start.setVisibility(View.GONE);
                recordvoice_stop.setVisibility(View.VISIBLE);

                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(true);
                ActivityCompat.requestPermissions
                        (RecordActivity.this,
                                new String[]{Manifest.permission.RECORD_AUDIO},
                                REQUEST_RECORD_PERMISSION);

            }
        });
        recordvoice_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordvoice_start.setVisibility(View.VISIBLE);
                recordvoice_stop.setVisibility(View.GONE);

                progressBar.setIndeterminate(false);
                progressBar.setVisibility(View.INVISIBLE);
                speech.stopListening();

            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getExtras();

        // This part works fine.
        ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);

        // But audioUri is always null !!!
        Uri audioUri = data.getData();
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            // mediaPlayer.setDataSource(String.valueOf(myUri));
            mediaPlayer.setDataSource(RecordActivity.this,audioUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        Log.e("wwww"," "+audioUri.getPath());
        ContentResolver contentResolver = getContentResolver();
        try {
            InputStream filestream = contentResolver.openInputStream(audioUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    speech.startListening(recognizerIntent);
                } else {
                    Toast.makeText(RecordActivity.this, "Permission Denied!", Toast
                            .LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speech != null) {
            speech.destroy();
            Log.i(LOG_TAG, "destroy");
        }
    }


    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
        progressBar.setIndeterminate(false);
        progressBar.setMax(10);
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
        progressBar.setVisibility(View.GONE);
        progressBar.setIndeterminate(true);
        recordvoice_start.setVisibility(View.VISIBLE);
        recordvoice_stop.setVisibility(View.GONE);

    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);

        Log.d(LOG_TAG, "FAILED " + errorMessage);
        progressBar.setVisibility(View.GONE);
        speech_tv.setText(errorMessage);
        recordvoice_start.setVisibility(View.VISIBLE);
        recordvoice_stop.setVisibility(View.GONE);
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(LOG_TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(LOG_TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";

        speech_tv.append(" " + text);
        App.getInstance().getDB().scheduleDao().update_msg(speech_tv.getText().toString(), String.valueOf(callLog_tb.getId()));
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
        progressBar.setProgress((int) rmsdB);
    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

//    class SaveAudioRecorder extends AsyncTask<String, String, String> {
//
//        int speechFlag = 0;
//
//        String AudioSavePathInDevice = null;
//        MediaRecorder mediaRecorder;
//        Random random;
//        String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
//        public static final int RequestPermissionCode = 1;
//        MediaPlayer mediaPlayer;
//
//        /**
//         * Before starting background thread
//         * Show Progress Bar Dialog
//         */
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            random = new Random();
//        }
//
//        /**
//         * Downloading file in background thread
//         */
//        @Override
//        protected String doInBackground(String... smilyCategory) {
//            int count;
//            startAudioRecorde();
//            return null;
//
//        }
//
//        private void startAudioRecorde() {
//
//
//            AudioSavePathInDevice =
//                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
//                            CreateRandomAudioFileName(5) + "AudioRecording.3gp";
//
//            MediaRecorderReady();
//
//            try {
//                mediaRecorder.prepare();
//                mediaRecorder.start();
//            } catch (IllegalStateException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//
////            Toast.makeText(this, "Recording started",
////                    Toast.LENGTH_LONG).show();
//
//        }
//
//
//        /**
//         * Updating progress bar
//         */
//        protected void onProgressUpdate(String... progress) {
//            // setting progress percentage
////            pDialog.setProgress(Integer.parseInt(progress[0]));
//        }
//
//        /**
//         * After completing background task
//         * Dismiss the progress dialog
//         **/
//        @Override
//        protected void onPostExecute(String file_url) {
//            // dismiss the dialog after the file was downloaded
//
//
//// emojiesTbArrayList  = emojiesSmiley;
////            emojiesTbArrayList.clear();
////            addMoreSmiliesAdapterList.notifyDataSetChanged();
//        }
//
//        private void stopRecorder() {
//
//            mediaRecorder.stop();
//            Toast.makeText(getApplicationContext(), "Recording Completed " +AudioSavePathInDevice,
//                    Toast.LENGTH_LONG).show();
//        }
//
//        public String CreateRandomAudioFileName(int string) {
//            StringBuilder stringBuilder = new StringBuilder(string);
//            int i = 0;
//            while (i < string) {
//                stringBuilder.append(RandomAudioFileName.
//                        charAt(random.nextInt(RandomAudioFileName.length())));
//
//                i++;
//            }
//            return stringBuilder.toString();
//        }
//
//        public void MediaRecorderReady() {
//            mediaRecorder = new MediaRecorder();
//            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//            mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
//            mediaRecorder.setOutputFile(AudioSavePathInDevice);
//        }
//
//    }
}
