package com.claysys.callapp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.LoginApiReq;
import com.claysys.callapp.rest.response.LoginResp;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements IResponseView {


    @BindView(R.id.login_btn)
    Button loginBtn;

    Boolean isNumberExist = false;
    FirebaseFirestore db;
    SharedPreferredManager sharedPreferredManager;
    @BindView(R.id.firstname_et)
    EditText firstnameEt;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.lastname_et)
    EditText lastnameEt;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.avi)
    com.wang.avi.AVLoadingIndicatorView avi;
    @BindView(R.id.forgotPasswordTextView)
    TextView forgotPasswordTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        db = FirebaseFirestore.getInstance();
        sharedPreferredManager = new SharedPreferredManager(this);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.mrmBackground));
        if (sharedPreferredManager.IsUser()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.login_btn)
    public void onViewClicked() {
        if (firstnameEt.getText().length() != 0) {
            if (lastnameEt.getText().length() != 0) {
                callLoginApi(firstnameEt.getText().toString(), lastnameEt.getText().toString());
            } else {
                lastnameEt.setError("Please mention a Password");
            }
        } else {
            firstnameEt.setError("Please enter a UserName");
        }
    }

    private void callLoginApi(String userName, String passWord) {
        startAnim();
        LoginApiReq loginApiReq = new LoginApiReq();
        loginApiReq.setUserName(userName);
        loginApiReq.setPassword(passWord);
        ApiHelper.getInstance().loginApiDetails(this, this, loginApiReq);
    }

    @Override
    public void onLoginApiSucess(LoginResp loginResp) {
        if (loginResp.getStatus() == null) {
            if (loginResp.getId() != null) {
                onLoginSucessFirebase(loginResp);
            } else {
                stopAnim();
                showingErrorDialog(null);
            }
        } else {
            stopAnim();
            showingErrorDialog(loginResp.getStatus());
        }
    }

    private void showingErrorDialog(String error) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_ok);
        dialog.getWindow().setLayout((int) getResources().getDimension(R.dimen._300sdp), (int) getResources().getDimension(R.dimen._120sdp));
        dialog.setCanceledOnTouchOutside(false);

        TextView okGotIt = dialog.findViewById(R.id.cancel);
        TextView errorTv = dialog.findViewById(R.id.error_tv);
        if (error != null) {
            errorTv.setText(error);
        }

        okGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    private void onLoginSucessFirebase(LoginResp loginResp) {
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("", document.getId() + " => " + document.getData());

                                if (String.valueOf(document.getData().get("userName")).equals(loginResp.getUserName())) {
                                    isNumberExist = true;
                                    sharedPreferredManager.setUserFirebaseId(document.getId());
                                    sharedPreferredManager.insertIsUser(true);
                                    sharedPreferredManager.setAccessToken(loginResp.getToken());
                                }

                            }
                            if (!isNumberExist) {
                                Map<String, Object> user = new HashMap<>();
//                                String[] splited = loginResp.getDisplayName().split("\\s+");
//                                for(int i=0;i<splited.length;i++){
//                                    if(i==0)
//
//                                    else if(i==1)
//                                        user.put("lastName", splited[i]);
//                                }
                                user.put("displayName", loginResp.getDisplayName());
                                if (loginResp.getId() != null)
                                    user.put("serverId", loginResp.getId());
//                                user.put("Email", "");
//                                user.put("mobile", "");
                                user.put("userName", loginResp.getUserName());
                                // Add a new document with a generated ID
                                db.collection("users")
                                        .add(user)
                                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                ContactTb contactTbReg = new ContactTb();
                                                Log.d("ssaj", "DocumentSnapshot added with ID: " + documentReference.getId());
                                                sharedPreferredManager.setUserFirebaseId(documentReference.getId());
                                                sharedPreferredManager.insertIsUser(true);
                                                sharedPreferredManager.setAccessToken(loginResp.getToken());
                                                contactTbReg.setFirstName(user.get("displayName").toString());
//                                                contactTbReg.setFirstName(user.get("firstName").toString());
                                                if (user.get("lastName") != null)
                                                    contactTbReg.setLastName(user.get("lastName").toString());
                                                if (user.get("Email") != null)
                                                    contactTbReg.setEmailID(user.get("Email").toString());
                                                if (user.get("mobile") != null)
                                                    contactTbReg.setMobile(user.get("mobile").toString());
                                                contactTbReg.setDocment_id(documentReference.getId());
//                                                setEmployeeApi(contactTbReg);
                                                stopAnim();
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();

                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w("ssaj", "Error adding document", e);
                                            }
                                        });
                            } else {
                                stopAnim();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            stopAnim();
                            Toast.makeText(LoginActivity.this,task.getException()+"",Toast.LENGTH_LONG).show();
                            Log.w("", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onLoginApiFailure() {
        stopAnim();

    }
    public void startAnim(){
        avi.setVisibility(View.VISIBLE);
        avi.show();
        // or avi.smoothToShow();
    }
    void stopAnim(){
        avi.hide();

        // or avi.smoothToHide();
    }
}
