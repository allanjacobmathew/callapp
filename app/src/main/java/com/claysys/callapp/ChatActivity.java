package com.claysys.callapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.adapter.ChatRoomAdapterList;
import com.claysys.callapp.adapter.SimpleContactListAdapter;
import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.models.GroupChatMsgs;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.SaveMsgDetails;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.AppConstants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements IResponseView, SimpleContactListAdapter.OnClickContactToTagtUser {


    SharedPreferredManager sharedPreferredManager;
    FirebaseFirestore db;
    ContactTb contactTb;
    String receiverId;
    String TAG = "chat";

    ArrayList<GroupChatMsgs> groupChatMsgsArrayList;
    ChatRoomAdapterList chatRoomAdapterList;
    @BindView(R.id.tv_toolbarTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.tv_lastSeen)
    TextView tvLastSeen;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.ll_center_name_img)
    RelativeLayout llCenterNameImg;
    @BindView(R.id.toolbarContainer)
    LinearLayout toolbarContainer;
    @BindView(R.id.chat_toolbar)
    Toolbar chatToolbar;
    @BindView(R.id.recyclerChat)
    RecyclerView recyclerChat;
    @BindView(R.id.im_attch_files)
    ImageView imAttchFiles;
    @BindView(R.id.editWriteMessage)
    EditText editWriteMessage;
    @BindView(R.id.ll_send_msg)
    LinearLayout llSendMsg;
    @BindView(R.id.btnSend)
    LinearLayout btnSend;
    @BindView(R.id.ll_send_container)
    LinearLayout llSendContainer;
    @BindView(R.id.button_end_chat)
    Button buttonEndChat;
//    int channelIsCreated = 1;

    String userId1, userId2;
    int taggedAUser = 0;

    String mainChannelDocumentId;
    SaveMsgDetails saveMsgStartDetails;
    SaveMsgDetails saveMsgEndDetails;
    int callingEndApi = 0;
    String senderServerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        contactTb = intent.getParcelableExtra("contactTb");
        sharedPreferredManager = new SharedPreferredManager(this);
        groupChatMsgsArrayList = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        setAdapter();

        tvToolbarTitle.setText(contactTb.getFirstName());
        tvLastSeen.setText(contactTb.getAlternate());
        getReceiverId();
        editWriteMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() == 0) {
                    btnSend.setEnabled(false);
                } else {
                    btnSend.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void setLiveDataFetchContact() {
        LiveData<List<ContactTb>> contacttbArrayList = App.getInstance().getDB().scheduleDao().getAllContactTb();

        contacttbArrayList.observe(this, new Observer<List<ContactTb>>() {
            @Override
            public void onChanged(List<ContactTb> contact_tbs) {
                ArrayList<ContactTb> contact_tbArrayList = (ArrayList<ContactTb>) contact_tbs;
                chatRoomAdapterList.setContactList(contact_tbArrayList);
            }
        });
    }

    private void getChannelDetailsOfUser() {
//        channelIsCreated = 1;
        String userId;
        if (contactTb.getIsUsersList()) {
            receiverId = contactTb.getDocment_id();
            userId = contactTb.getDocment_id();
            userId1 = "userId1";
            userId2 = "userId2";
        } else {

            if (contactTb.getMemberid1().equals(sharedPreferredManager.getUserFirebaseId())) {
                receiverId = contactTb.getMemberid2();
                userId = contactTb.getMemberid2();
                userId1 = "userId1";
                userId2 = "userId2";
            } else {
                receiverId = contactTb.getMemberid1();
                userId = contactTb.getMemberid1();
                userId1 = "userId2";
                userId2 = "userId1";
            }
        }
        String d = sharedPreferredManager.getUserFirebaseId().toString();

        db.collection(AppConstants.channel)
                .whereEqualTo(userId1, sharedPreferredManager.getUserFirebaseId())
                .whereEqualTo(userId2, userId)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {

                        int flag = 0;
                        if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            return;
                        }
                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            Log.w(TAG, "listen:error");
                            flag = 1;
                            mainChannelDocumentId = dc.getDocument().getId();
                            if (!groupChatMsgsArrayList.isEmpty()) {
                                if (groupChatMsgsArrayList.get(0).getMsgDocumentId() != null) {
                                    if(dc.getDocument().get("lastMessageId")!=null) {
                                        boolean containLastMsg = filter(dc.getDocument().get("lastMessageId").toString(), groupChatMsgsArrayList);
                                        if (containLastMsg) {
                                            if (editWriteMessage.getText().toString().length() != 0) {
                                                sendMsgToUser(dc.getDocument().getId());
                                            }
                                        } else {
                                            settingAllMsgs();
                                        }
                                    }
                                } else {
                                    sendMsgToUser(dc.getDocument().getId());
                                }
                            } else {
                                sendMsgToUser(dc.getDocument().getId());
                            }
                        }
                        if (flag == 0) {
                            channelCreation();
                        }

                    }
                });


    }

    private void getReceiverId() {
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (sharedPreferredManager.getUserFirebaseId().equals(document.getId())) {
                                    senderServerId = document.getData().get("serverId").toString();
                                }
                            }
                        } else {

                            Log.w("", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void setAdapter() {

        chatRoomAdapterList = new ChatRoomAdapterList(this, groupChatMsgsArrayList, this::onClickContactToTag);
        setLiveDataFetchContact();
        RecyclerView.LayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        recyclerChat.setLayoutManager(mLayoutManager);
        recyclerChat.setItemAnimator(new DefaultItemAnimator());
        recyclerChat.setAdapter(chatRoomAdapterList);
        recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
    }

    public void showEndChatDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_end_chat);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);

        Button endChat = dialog.findViewById(R.id.end_chat);
        Button cancel = dialog.findViewById(R.id.cancel);
        TextView tvEndChat = dialog.findViewById(R.id.tv_end_chat);
        if (contactTb.getUserDetails() != null) {
            if (contactTb.getUserDetails().getLastName() != null)
                tvEndChat.setText(getResources().getString(R.string.endchatwith) + " " + contactTb.getUserDetails().getFirstName() + " " + contactTb.getUserDetails().getLastName() + " ?");
            else if (contactTb.getUserDetails().getFirstName() != null)
                tvEndChat.setText(getResources().getString(R.string.endchatwith) + " " + contactTb.getUserDetails().getFirstName() + " ?");
        } else {
            if (contactTb.getLastName() != null)
                tvEndChat.setText(getResources().getString(R.string.endchatwith) + " " + contactTb.getFirstName() + " " + contactTb.getLastName() + " ?");
            else if (contactTb.getFirstName() != null)
                tvEndChat.setText(getResources().getString(R.string.endchatwith) + " " + contactTb.getFirstName() + " ?");
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        endChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                setEndChat();
            }
        });
        dialog.show();

    }

    private void setEndChat() {
        String msgDocumentId = groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getMsgDocumentId();
        String channelId = groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getChannelId();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("isEnd", true);
        DocumentReference washingtonRef = db.collection(AppConstants.channel + "/" + channelId + "/messages").document(msgDocumentId);
        washingtonRef
                .update(user1)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callEndChatApi(groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1));
                        groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).setEnd(true);
                        recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
                        chatRoomAdapterList.notifyDataSetChanged();
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating document", e);
                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();
        settingAllMsgs();
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.darkblue));
        setSupportActionBar(chatToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendMsgToUser(String id) {

        Calendar calendar = Calendar.getInstance();
//            Date dateTime = FieldValue.serverTimestamp();


        Date date22 = null;
        String dtStart = FieldValue.serverTimestamp().toString();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            date22 = format.parse(dtStart);
            System.out.println(date22);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String msgdata = editWriteMessage.getText().toString();

        Date now2 = new Date();

        Map<String, Object> user = new HashMap<>();
        user.put("channelId", id);
        user.put("senderFirId", sharedPreferredManager.getUserFirebaseId());
        user.put("receiverId", receiverId);
        String msg =  editWriteMessage.getText().toString();
        msg  = msg.replace("\r", "");
        msg  = msg.replace("\n", "");
        user.put("content", msg);
        user.put("sendServerDate", FieldValue.serverTimestamp());
        user.put("sendDate", calendar.getTimeInMillis());
        user.put("senderId", senderServerId);
        if (!groupChatMsgsArrayList.isEmpty()) {
            if (groupChatMsgsArrayList.size() > 0) {
                if (!groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).isEnd()) {
                    if (groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getTaggedUserId() != null) {
                        user.put("taggedUserId", groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getTaggedUserId());
                        if (groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getStartMsgSerevrId() != null) {
                            user.put("serverTagId", groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getStartMsgSerevrId());
                        }
                    }
                }
            }
        }

        if (taggedAUser == 1) {
            user.put("isStart", true);
            user.put("taggedUserId", groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).getUserToBeTagged());
            taggedAUser = 0;
            groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).setUserToBeTagged(null);
        }

        if (editWriteMessage.getText().toString() != "") {
            // Add a new document with a generated ID
            db.collection(AppConstants.channel + "/" + id + "/messages")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("ssaj", "DocumentSnapshot added with ID: " + documentReference.getId());
//                                editWriteMessage.setText("");
                            Map<String, Object> user1 = new HashMap<>();
                            user1.put("lastMessageContent", msgdata);
                            user1.put("lastMessageId", documentReference.getId());
                            user1.put("lastUpdateDate", FieldValue.serverTimestamp());
//                                channelIsCreated = 0;
                            Log.e("gdfg", "fdg" + groupChatMsgsArrayList);
                            if (groupChatMsgsArrayList.get(0).getMsgDocumentId() != null) {
                                boolean containMsgDocumentId = filter(documentReference.getId(), groupChatMsgsArrayList);
                                if (!containMsgDocumentId) {
                                    if (groupChatMsgsArrayList.get(0).getChannelId() == null) {
                                        if (groupChatMsgsArrayList.get(0).isEnd()) {
                                            groupChatMsgsArrayList.remove(0);
                                        }
                                    }
                                    GroupChatMsgs groupChatMsgs = new GroupChatMsgs();
                                    groupChatMsgs.setMsg(user.get("content").toString());
                                    groupChatMsgs.setMsgSenderId(sharedPreferredManager.getUserFirebaseId());
                                    groupChatMsgs.setMsgReceiverId(receiverId);
                                    groupChatMsgs.setChannelId(id);
                                    if (user.get("serverTagId") != null) {
                                        groupChatMsgs.setStartMsgSerevrId(user.get("serverTagId").toString());
                                    }
                                    if (user.get("taggedUserId") != null) {
                                        groupChatMsgs.setTaggedUserId(user.get("taggedUserId").toString());
                                    }
                                    groupChatMsgs.setMsgDocumentId(documentReference.getId());
                                    if (user.get("isStart") != null) {
                                        groupChatMsgs.setStart(Boolean.parseBoolean(user.get("isStart").toString()));
                                        groupChatMsgs.setUserToBeTagged(null);
                                        groupChatMsgs.setTaggedUserId(user.get("taggedUserId").toString());
                                        callStartChatApi(groupChatMsgs);

                                    }
                                    groupChatMsgsArrayList.add(groupChatMsgs);
                                    recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
                                    chatRoomAdapterList.notifyDataSetChanged();
                                }
                            } else {
//                                if (groupChatMsgsArrayList.get(0).isEnd()) {
//                                    groupChatMsgsArrayList.remove(0);
//                                }
                                if (groupChatMsgsArrayList.get(0).getChannelId() == null) {
                                    if (groupChatMsgsArrayList.get(0).isEnd()) {
                                        groupChatMsgsArrayList.remove(0);
                                    }
                                }
                                GroupChatMsgs groupChatMsgs = new GroupChatMsgs();
                                groupChatMsgs.setMsg(user.get("content").toString());
                                groupChatMsgs.setMsgReceiverId(receiverId);
                                groupChatMsgs.setMsgSenderId(sharedPreferredManager.getUserFirebaseId());
                                groupChatMsgs.setChannelId(id);
                                groupChatMsgs.setMsgDocumentId(documentReference.getId());
                                if (user.get("serverTagId") != null) {
                                    groupChatMsgs.setStartMsgSerevrId(user.get("serverTagId").toString());
                                }
                                if (user.get("taggedUserId") != null) {
                                    groupChatMsgs.setTaggedUserId(user.get("taggedUserId").toString());
                                }
                                if (user.get("isStart") != null) {
                                    groupChatMsgs.setStart(Boolean.parseBoolean(user.get("isStart").toString()));
                                    groupChatMsgs.setUserToBeTagged(null);
                                    groupChatMsgs.setTaggedUserId(user.get("taggedUserId").toString());
                                    callStartChatApi(groupChatMsgs);
                                }

                                groupChatMsgsArrayList.add(groupChatMsgs);
                                recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
                                chatRoomAdapterList.notifyDataSetChanged();
                            }
                            DocumentReference washingtonRef = db.collection(AppConstants.channel).document(id);
                            washingtonRef
                                    .update(user1)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "DocumentSnapshot successfully updated!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error updating document", e);
                                        }
                                    });


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("ssaj", "Error adding document", e);
                        }
                    });

            editWriteMessage.setText("");
        }


    }


    private void settingAllMsgs() {
        if (mainChannelDocumentId == null) {
            mainChannelDocumentId = contactTb.getDocment_id();
        }
        db.collection(AppConstants.channel + "/" + mainChannelDocumentId + "/messages")
                .orderBy("sendServerDate", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:

                                    GroupChatMsgs groupChatMsgs = new GroupChatMsgs();
                                    groupChatMsgs.setMsg(dc.getDocument().getData().get("content").toString());
                                    groupChatMsgs.setMsgSenderId(dc.getDocument().getData().get("senderFirId").toString());
                                    groupChatMsgs.setChannelId(dc.getDocument().getData().get("channelId").toString());
                                    groupChatMsgs.setMsgDocumentId(dc.getDocument().getId());
                                    if (dc.getDocument().getData().get("serverTagId") != null)
                                        groupChatMsgs.setStartMsgSerevrId(dc.getDocument().getData().get("serverTagId").toString());
                                    if (dc.getDocument().get("isStart") != null) {
                                        groupChatMsgs.setStart(Boolean.parseBoolean(dc.getDocument().get("isStart").toString()));
                                        groupChatMsgs.setUserToBeTagged(null);
                                        if (dc.getDocument().get("taggedUserId") != null) {
                                            groupChatMsgs.setTaggedUserId(dc.getDocument().get("taggedUserId").toString());
                                        }
                                        if (dc.getDocument().getData().get("serverTagId") != null) {
                                            groupChatMsgs.setStartMsgSerevrId(dc.getDocument().getData().get("serverTagId").toString());
                                        } else {
                                            if (dc.getDocument().getData().get("senderFirId").toString().equals(sharedPreferredManager.getUserFirebaseId())) {
                                                callStartChatApi(groupChatMsgs);
                                            }
                                        }
                                    }
                                    if (dc.getDocument().get("taggedUserId") != null) {
                                        groupChatMsgs.setTaggedUserId(dc.getDocument().get("taggedUserId").toString());
                                    }
                                    if (dc.getDocument().get("isEnd") != null) {
                                        groupChatMsgs.setEnd(Boolean.parseBoolean(dc.getDocument().get("isEnd").toString()));

                                    }
                                    if (groupChatMsgsArrayList.size() > 0) {
                                        if (groupChatMsgsArrayList.get(0).getMsgDocumentId() != null) {
                                            boolean containMsgDocumentId = filter(dc.getDocument().getId(), groupChatMsgsArrayList);
                                            if (!containMsgDocumentId) {
                                                groupChatMsgsArrayList.add(groupChatMsgs);
                                            }
                                        } else {
                                            groupChatMsgsArrayList.add(groupChatMsgs);
                                        }
                                    } else {
                                        groupChatMsgsArrayList.add(groupChatMsgs);
                                    }
                                    Timestamp timestamp = (Timestamp) dc.getDocument().getData().get("sendServerDate");
                                    if (timestamp != null) {
                                        groupChatMsgs.setCreatedDateTimeStamp(String.valueOf(timestamp.getSeconds()));
                                    }
                                    Log.d("timestamp=====", " " + dc.getDocument().getData().get("Timestamp"));
//                                    long id = App.getInstance().getDB().scheduleDao().insertGroupChats(groupChatMsgs);
                                    Log.d(TAG, "New city: " + dc.getDocument().getData());

                                    break;
                                case MODIFIED:
                                    if (dc.getDocument().getData().get("serverTagId") != null)
                                        groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).setStartMsgSerevrId(dc.getDocument().getData().get("serverTagId").toString());
                                    if (dc.getDocument().get("isEnd") != null)
                                        groupChatMsgsArrayList.get(groupChatMsgsArrayList.size() - 1).setEnd(Boolean.parseBoolean(dc.getDocument().get("isEnd").toString()));
                                    Log.d(TAG, "Modified city: " + dc.getDocument().getData());
                                    break;
                                case REMOVED:
                                    Log.d(TAG, "Removed city: " + dc.getDocument().getData());
                                    break;
                            }
                        }
                        recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
                        chatRoomAdapterList.notifyDataSetChanged();
                        if (groupChatMsgsArrayList.isEmpty()) {
                            GroupChatMsgs groupChatMsgs = new GroupChatMsgs();
                            groupChatMsgs.setEnd(true);
                            groupChatMsgsArrayList.add(groupChatMsgs);
                            setAdapter();
                        } else {
                            if (groupChatMsgsArrayList.get(0).getChannelId() == null) {
                                if (groupChatMsgsArrayList.get(0).isEnd()) {
                                    groupChatMsgsArrayList.remove(0);
                                }
                            }
//                            if (groupChatMsgsArrayList.get(0).isEnd()) {
//                                groupChatMsgsArrayList.remove(0);
//                            }
                        }
                    }
                });


    }


    @OnClick({R.id.button_end_chat, R.id.btnSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_end_chat:
                showEndChatDialog();
                break;
            case R.id.btnSend:
                getChannelDetailsOfUser();

                break;
        }

    }


    public void channelCreation() {

        Map<String, Object> user = new HashMap<>();
        user.put("userId1", sharedPreferredManager.getUserFirebaseId());
        user.put("userId2", contactTb.getDocment_id());
        user.put("lastMessageContent", "");
        user.put("lastMessageId", "");
        user.put("lastUpdateDate", "");
        user.put("name", "New Channel");
        user.put("type", "single");


// Add a new document with a generated ID
        db.collection(AppConstants.channel)
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
//                        channelIsCreated = 0;
                        Log.d("ssaj", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
//                        channelIsCreated = 0;
                        Log.w("ssaj", "Error adding document", e);
                    }
                });

    }


    @Override
    public void onClickContactToTag(ContactTb contactTb, int chatRoomMsgPosition) {
        taggedAUser = 1;

//        groupChatMsgsArrayList.get(chatRoomMsgPosition).setEnd(true);
//        groupChatMsgsArrayList.get(chatRoomMsgPosition).setStart(true);
        groupChatMsgsArrayList.get(chatRoomMsgPosition).setUserToBeTagged(contactTb.getId());
//        groupChatMsgsArrayList.get(chatRoomMsgPosition).setTaggedUserId(contactTb.getId());
//        groupChatMsgsArrayList.get(chatRoomMsgPosition).setTaggedUserName(contactTb.getFirstName() + " " + contactTb.getLastName());
        recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
        chatRoomAdapterList.notifyDataSetChanged();
    }

    public boolean filter(String id, ArrayList<GroupChatMsgs> groupChatMsgsList) {
        //new array list that will hold the filtered data
        ArrayList<GroupChatMsgs> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (GroupChatMsgs s : groupChatMsgsList) {
            //if the existing elements contains the search input
            if (s.getMsgDocumentId().contains(id)) {
                GroupChatMsgs contactDetails = new GroupChatMsgs();
                contactDetails = s;
                //adding the element to filtered list
                filterdNames.add(contactDetails);

            }
        }
        if (filterdNames.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void callEndChatApi(GroupChatMsgs groupChatMsgs) {
        callingEndApi = 1;
        saveMsgEndDetails = new SaveMsgDetails();
        if (groupChatMsgs.getStartMsgSerevrId() != null) {
            saveMsgEndDetails.setId(Integer.parseInt(groupChatMsgs.getStartMsgSerevrId()));
            saveMsgEndDetails.setEndMessageFIRId(groupChatMsgs.getMsgDocumentId());
        }
        ApiHelper.getInstance().sendEndMsgDetails(this, this, saveMsgEndDetails);
    }

    //
    private void callStartChatApi(GroupChatMsgs groupChatMsgs) {
        saveMsgStartDetails = new SaveMsgDetails();
        saveMsgStartDetails.setChannelFIRId(groupChatMsgs.getChannelId());
        saveMsgStartDetails.setStartMessageFIRId(groupChatMsgs.getMsgDocumentId());
        saveMsgStartDetails.setTaggedMemberId(groupChatMsgs.getTaggedUserId());
        saveMsgStartDetails.setFirstEmployeeFIRId(groupChatMsgs.getMsgSenderId());
        saveMsgStartDetails.setSecondEmployeeFIRId(groupChatMsgs.getMsgReceiverId());
        Log.e("fhsdj", "skdufhus" + groupChatMsgs);
        ApiHelper.getInstance().sendStartMsgDetails(this, this, saveMsgStartDetails);
    }


    @Override
    public void onSaveCallLogsSucess(SaveCallLogResp saveCallLogResp) {
        if (callingEndApi == 0) {
            if (saveCallLogResp.getStatus().equals(AppConstants.Success)) {
                for (int i = 0; i < groupChatMsgsArrayList.size(); i++) {
                    if (groupChatMsgsArrayList.get(i).getMsgDocumentId().equals(saveMsgStartDetails.getStartMessageFIRId())) {
                        setStartMsgSerevrIdChat(groupChatMsgsArrayList.get(i), i, String.valueOf(saveCallLogResp.getId()));
                    }
                }
            }
        } else {
            callingEndApi = 0;
        }
    }

    @Override
    public void onSaveCallLogsFailure() {

    }

    private void setStartMsgSerevrIdChat(GroupChatMsgs groupChatMsgs, int position, String startMsgSerevrId) {
        String msgDocumentId = groupChatMsgs.getMsgDocumentId();
        String channelId = groupChatMsgs.getChannelId();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("serverTagId", startMsgSerevrId);
        DocumentReference washingtonRef = db.collection(AppConstants.channel + "/" + channelId + "/messages").document(msgDocumentId);
        washingtonRef
                .update(user1)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        groupChatMsgsArrayList.get(position).setStartMsgSerevrId(startMsgSerevrId);
                        recyclerChat.scrollToPosition(groupChatMsgsArrayList.size() - 1);
                        chatRoomAdapterList.notifyDataSetChanged();
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating document", e);
                    }
                });
    }

    public void setVisibleOfSendContainer(boolean visibleOfSendContainer) {
        if (visibleOfSendContainer) {
//            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) llSendContainer.getLayoutParams();
//            lp.addRule(RelativeLayout.ABOVE, recyclerChat.getId());
            llSendContainer.setVisibility(View.VISIBLE);
            buttonEndChat.setVisibility(View.VISIBLE);
//            RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//
//            layoutParams.setMargins(0, 0, 0, 20);
//            recyclerChat.setLayoutParams(layoutParams);
        } else {
            hideSoftKeyboard();
            llSendContainer.setVisibility(View.GONE);
            buttonEndChat.setVisibility(View.INVISIBLE);
        }
    }

    public void hideSoftKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @OnClick(R.id.editWriteMessage)
    public void onViewClicked() {
//        Toast.makeText(this,"clicked",Toast.LENGTH_LONG).show();
    }
}

