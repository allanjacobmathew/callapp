package com.claysys.callapp.apicontroller;

import android.content.Context;

import com.claysys.callapp.App;
import com.claysys.callapp.helper.DisposableManager;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.LoginApiReq;
import com.claysys.callapp.rest.request.SaveCallLogNoteReq;
import com.claysys.callapp.rest.request.SaveCallLogReq;
import com.claysys.callapp.rest.request.SaveMsgDetails;
import com.claysys.callapp.rest.responsehandler.FetchAllEmployeesListHandl;
import com.claysys.callapp.rest.responsehandler.GetAllContactListRespHandler;
import com.claysys.callapp.rest.responsehandler.LoginRespHandl;
import com.claysys.callapp.rest.responsehandler.SaveCallLogRespHandl;
import com.claysys.callapp.rest.responsehandler.SaveCallLogRespHandlFile;
import com.claysys.callapp.rest.service.ApiClient;
import com.claysys.callapp.rest.service.ApiInterface;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.AppConstants;

import java.io.File;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by susan sajeev on 22-10-2018
 */

public class ApiHelper {
    public static ApiHelper apiHelper;
    public static ApiInterface apiInterface;
    public static ApiInterface apiInterfaceForPurchase;

    SharedPreferredManager sharedPreferredManager;

    public static ApiHelper getInstance() {

        if (apiHelper == null) {
            apiHelper = new ApiHelper();
            apiInterface = new ApiClient().getRetrofit().create(ApiInterface.class);
//
        }
        return apiHelper;

    }

    public static void dispose() {
        apiHelper = null;
    }

    // GetAllContact
    public void getAllContactsListApi(Context context, IResponseView responseView) {
        sharedPreferredManager = new SharedPreferredManager(context);
        Disposable disposable = apiInterface.getAllContacts(AppConstants.Bearer +sharedPreferredManager.getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new GetAllContactListRespHandler(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }
    // SaveCallLog
    public void saveCallLogApi(Context context, IResponseView responseView, SaveCallLogReq saveCallLogReq) {
        Disposable disposable = apiInterface.saveCallLogs(AppConstants.Bearer +sharedPreferredManager.getAccessToken(),saveCallLogReq)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }


    // SaveCallLogNOtes
    public void saveCallLogNoteApi(Context context, IResponseView responseView, SaveCallLogNoteReq saveCallLogNoteReq) {
        Disposable disposable = apiInterface.saveCallLogNotes(AppConstants.Bearer +sharedPreferredManager.getAccessToken(),saveCallLogNoteReq)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }


    // SaveAudioFiles
    public void saveAudioFilesApi(Context context, IResponseView responseView, File file) {
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("audio/*"), file));

        Disposable disposable = apiInterface.saveAudioUpload(AppConstants.Bearer +sharedPreferredManager.getAccessToken(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandlFile(context, responseView, AppConstants.SAVEAUDIOFILES));
        DisposableManager.add(disposable);
    }

    //registerUser
    public void addEmploye(Context context, IResponseView responseView, ContactTb contactTb) {
        Disposable disposable = apiInterface.addEmployee(AppConstants.Bearer +sharedPreferredManager.getAccessToken(),contactTb)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }
    //FetchAllEmployeees
    public void getAllEmployeees(Context context, IResponseView responseView, String value) {
        Disposable disposable = apiInterface.fetchAllEmployees(AppConstants.Bearer +sharedPreferredManager.getAccessToken(), value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new FetchAllEmployeesListHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }

    //sendStartMsgDetails
    public void sendStartMsgDetails(Context context, IResponseView responseView, SaveMsgDetails saveMsgDetails) {
        Disposable disposable = apiInterface.sendStartMsg(AppConstants.Bearer +sharedPreferredManager.getAccessToken(), saveMsgDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }


    //sendEndMsgDetails
    public void sendEndMsgDetails(Context context, IResponseView responseView, SaveMsgDetails saveMsgDetails) {
        Disposable disposable = apiInterface.sendStoptMsg(AppConstants.Bearer +sharedPreferredManager.getAccessToken(), saveMsgDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveCallLogRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }

    public void loginApiDetails(Context context, IResponseView responseView, LoginApiReq loginApiReq) {
        Disposable disposable = apiInterface.login(loginApiReq)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new LoginRespHandl(context, responseView, AppConstants.GETALLCONTACTS));
        DisposableManager.add(disposable);
    }


}
