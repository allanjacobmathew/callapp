package com.claysys.callapp.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.claysys.callapp.utlis.AppConstants;



public class SharedPreferredManager {
    // Shared Preferences reference
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREFER_NAME = "MRM";

    public SharedPreferredManager(Context _context) {
        this._context = _context;
        if(_context!=null) {
            pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }
    }

    // Editor reference for Shared preferences
    public void setUserName(String userName) {
        editor.putString(AppConstants.Username, userName);
        editor.commit();
    }
    public String getUserName() {
        return pref.getString(AppConstants.Username, null);
    }

    public String getEmailId() {
        return pref.getString(AppConstants.EmailId, null);
    }
    public void setEmailId(String emailId) {
        editor.putString(AppConstants.EmailId, emailId);
        editor.commit();
    }


    //setToken
    public String getDeviceToken() {
        return pref.getString(AppConstants.deviceToken, null);
    }
    public void setDeviceToken(String  deviceToken) {
        editor.putString(AppConstants.deviceToken, deviceToken);
        editor.commit();
    }

    public String getAccessToken() {
        return pref.getString(AppConstants.AccessToken, null);
    }
    public void setAccessToken(String  accessToken) {
        editor.putString(AppConstants.AccessToken, accessToken);
        editor.commit();
    }

    public void insertRefreshToken(String refreshToken) {
        editor.putString(AppConstants.RefreshedToken, refreshToken);
        editor.commit();
    }
    public String getRefreshToken() {
        return pref.getString(AppConstants.RefreshedToken, null);
    }

    public void setUserLocalId(int userId) {
        editor.putInt(AppConstants.UserLocalId, userId);
        editor.commit();
    }
    public int getUserLocalId() {
        return pref.getInt(AppConstants.UserLocalId, 0);
    }

    public Long getUserMainId() {
        return pref.getLong(AppConstants.UserMainId, 0);
    }

    public void setUsermainId(long userId) {
        editor.putLong(AppConstants.UserMainId, userId);
        editor.commit();
    }

    public Long getGUId() {
        return pref.getLong(AppConstants.GUUID, 0);
    }

    public void setGUId(long guid) {
        editor.putLong(AppConstants.GUUID, guid);
        editor.commit();
    }


    public  void insertRefreshTokenStatus(boolean refreshTokenStatus) {
        editor.putBoolean(AppConstants.HAS_REFRESH_TOKEN, refreshTokenStatus);
        editor.commit();
    }

    public  void insertTokenStatus(boolean tokenStatus) {
        editor.putBoolean(AppConstants.HAS_TOKEN, tokenStatus);
        editor.commit();

    }
    public  boolean hasToken( ) {
        return pref.getBoolean(AppConstants.HAS_TOKEN, false);
    }

    public  boolean hasRefreshToken( ) {
        return pref.getBoolean(AppConstants.HAS_REFRESH_TOKEN, false);
    }

    public void insertDeviceIdApi(String deviceId) {
        editor.putString(AppConstants.DeviceIdApi, deviceId);
        editor.commit();
    }
    public String getDeviceIdApi() {
        return pref.getString(AppConstants.DeviceIdApi, null);
    }


    public  void insertIsUser(boolean tokenStatus) {
        editor.putBoolean(AppConstants.IsUser, tokenStatus);
        editor.commit();

    }
    public  boolean IsUser( ) {
        boolean b = pref.getBoolean(AppConstants.IsUser, false);
        return pref.getBoolean(AppConstants.IsUser, false);
    }


    public void setUserFirebaseId(String userIdFirebase) {
        editor.putString(AppConstants.UserFirebaseId, userIdFirebase);
        editor.commit();
    }
    public String getUserFirebaseId() {
        return pref.getString(AppConstants.UserFirebaseId, null);
    }


}






