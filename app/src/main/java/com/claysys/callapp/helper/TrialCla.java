//package com.claysys.callapp.helper;
//
//import android.Manifest;
//import android.content.ContentResolver;
//import android.content.ContextWrapper;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.media.MediaPlayer;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.speech.RecognitionListener;
//import android.speech.RecognizerIntent;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//import androidx.core.content.ContextCompat;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import androidx.fragment.app.FragmentTransaction;
//
//import com.claysys.callapp.App;
//import com.claysys.callapp.FragmentId;
//import com.claysys.callapp.MainActivity;
//import com.claysys.callapp.R;
//import com.claysys.callapp.apicontroller.ApiHelper;
//import com.claysys.callapp.fragment.CallLogFragment;
//import com.claysys.callapp.fragment.ContactFragment;
//import com.claysys.callapp.rest.interfaceviews.IResponseView;
//import com.claysys.callapp.rest.response.GetAllContactResp;
//import com.claysys.callapp.services.GetCallLogsDetails;
//import com.claysys.callapp.sqllite.helper.CallLogTb;
//import com.claysys.callapp.utlis.AppConstants;
//import com.claysys.callapp.utlis.GrantingPermission;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.SequenceInputStream;
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//public class TrialCla {
//    package com.claysys.callapp;
//
//import android.Manifest;
//import android.content.ContentResolver;
//import android.content.ContextWrapper;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.media.MediaPlayer;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.speech.RecognitionListener;
//import android.speech.RecognizerIntent;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//import androidx.core.content.ContextCompat;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import androidx.fragment.app.FragmentTransaction;
//
//import com.claysys.callapp.apicontroller.ApiHelper;
//import com.claysys.callapp.fragment.CallLogFragment;
//import com.claysys.callapp.fragment.ContactFragment;
//import com.claysys.callapp.rest.interfaceviews.IResponseView;
//import com.claysys.callapp.rest.response.GetAllContactResp;
//import com.claysys.callapp.services.GetCallLogsDetails;
//import com.claysys.callapp.sqllite.helper.CallLogTb;
//import com.claysys.callapp.utlis.AppConstants;
//import com.claysys.callapp.utlis.GrantingPermission;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.SequenceInputStream;
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//    public class MainActivity extends AppCompatActivity implements IResponseView, RecognitionListener {
//
//        @BindView(R.id.ll_container_layout)
//        FrameLayout llContainerLayout;
//        @BindView(R.id.im_contact)
//        ImageView imContact;
//        @BindView(R.id.ll_contact)
//        LinearLayout llContact;
//        @BindView(R.id.im_calllog)
//        ImageView imCalllog;
//        @BindView(R.id.ll_calllog)
//        LinearLayout llCalllog;
//        @BindView(R.id.ll_bottom_navigation)
//        LinearLayout llBottomNavigation;
//        @BindView(R.id.top_parent)
//        RelativeLayout topParent;
//        @BindView(R.id.iconMain)
//        TextView iconMain;
//        @BindView(R.id.tv_fragment_name)
//        TextView tvFragmentName;
//        @BindView(R.id.header)
//        LinearLayout header;
//
//        @BindView(R.id.im_fav)
//        ImageView imFav;
//        @BindView(R.id.ll_fav)
//        LinearLayout llFav;
//        @BindView(R.id.toolbar)
//        Toolbar toolbar;
//
//        private FragmentManager fragmentManager;
//        ContactFragment contactFragment;
//        CallLogTb mainCallLog;
//        Uri mainUriTest;
//
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        protected void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_main);
//            ButterKnife.bind(this);
//            getConatctsApi();
//
//            fragmentManager = getSupportFragmentManager();
//            onFragmentChangeListener(FragmentId.CONTACTFRAGMENT, false, null);
//            int permissionCallPhone12 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
//            if (permissionCallPhone12 != PackageManager.PERMISSION_GRANTED) {
//                new GrantingPermission().askForPermission(this, Manifest.permission.READ_CONTACTS, AppConstants.READ_CONTACTS);
//            }
//
//
//        }
//
//        public void setCallogLogList(CallLogTb callLogTb) {
//            this.mainCallLog = callLogTb;
//
//        }
//
//
//        @Override
//        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//            super.onActivityResult(requestCode, resultCode, data);
//            if (data != null) {
//                Bundle bundle = data.getExtras();
//
//                // This part works fine.
//                ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
//
//                // But audioUri is always null !!!
//                Uri audioUri = data.getData();
//
//                MediaPlayer mediaPlayer = new MediaPlayer();
//                try {
//                    // mediaPlayer.setDataSource(String.valueOf(myUri));
//                    mediaPlayer.setDataSource(com.claysys.callapp.MainActivity.this, audioUri);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    mediaPlayer.prepare();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                mediaPlayer.start();
//                Log.e("wwww", " " + audioUri.getPath());
//                ContentResolver contentResolver = getContentResolver();
//                InputStream filestream = null;
//                try {
//                    filestream = contentResolver.openInputStream(audioUri);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//                CallLogTb callLogTbFromDb = App.getInstance().getDB().scheduleDao().getMsg(mainCallLog.getId());
//                if (callLogTbFromDb.getAudioFileUrl() != null) {
//                    Uri audioLocalUri = saveAudioToInternalStorage(filestream, mainCallLog.getDate() + "tomerger");
////                mergeSongs(new File(mainUriTest.getPath()), new File(audioLocalUri.getPath()));
//                    InputStream filestreamPrevious = null;
//                    InputStream filestreamCurrent = null;
//                    try {
//                        Uri newUri = Uri.fromFile(new File(mainUriTest.getPath()));
//                        filestreamPrevious = contentResolver.openInputStream(newUri);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        Uri newUri = Uri.fromFile(new File(audioLocalUri.getPath()));
//                        filestreamCurrent = contentResolver.openInputStream(newUri);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//
////                mergingTwoFiles(filestreamPrevious, filestreamCurrent, "afterMergerAudioFile");
//                } else {
//                    Uri audioLocalUri = saveAudioToInternalStorage(filestream, mainCallLog.getDate() + mainCallLog.getId());
//                    mainUriTest = audioLocalUri;
//                    long id = App.getInstance().getDB().scheduleDao().updateAudioUrl(audioLocalUri.getPath(), mainCallLog.getId());
//                }
//            }
//
//        }
//
//        private void mergeSongs(File mergedFile, File... mp3Files) {
//            FileInputStream fisToFinal = null;
//            FileOutputStream fos = null;
//            try {
//                fos = new FileOutputStream(mergedFile);
//                fisToFinal = new FileInputStream(mergedFile);
//                for (File mp3File : mp3Files) {
//                    if (!mp3File.exists())
//                        continue;
//                    FileInputStream fisSong = new FileInputStream(mp3File);
//                    SequenceInputStream sis = new SequenceInputStream(fisToFinal, fisSong);
//                    byte[] buf = new byte[1024];
//                    try {
//                        for (int readNum; (readNum = fisToFinal.read(buf)) != -1; )
//                            fos.write(buf, 0, readNum);
////
//                    } finally {
//                        if (fisSong != null) {
//                            fisSong.close();
//                        }
//                        if (sis != null) {
//                            sis.close();
//                        }
//                    }
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    if (fos != null) {
//                        fos.flush();
//                        fos.close();
//                    }
//                    if (fisToFinal != null) {
//                        fisToFinal.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
////        protected Uri mergingTwoFiles(InputStream previousInput, InputStream inputCurrent, String name) {
////            // Initialize ContextWrapper
////            ContextWrapper wrapper = new ContextWrapper(getApplicationContext());
////            // Initializing a new file
////            // The bellow line return a directory in internal storage
////            File file = wrapper.getDir("Audios", MODE_PRIVATE);
////            // Create a file to save the image
////            file = new File(file, name + ".mp3");
////            SequenceInputStream sis = new SequenceInputStream(previousInput, inputCurrent);
////            try {
////                // Initialize a new OutputStream
////                OutputStream stream = null;
////                // If the output file exists, it can be replaced or appended to it
////                stream = new FileOutputStream(file);
////                try {
//////                try (OutputStream output = new FileOutputStream(file)) {
////                    byte[] buffer = new byte[6 * 1024];
////                    // or other buffer size
////                    int read;
////                    while ((read = previousInput.read(buffer)) != -1) {
////                        stream.write(buffer, 0, read);
////                    }
////                    while ((read = previousInput.read(buffer)) != -1) {
////                        stream.write(buffer, 0, read);
////                    }
////                    stream.flush();
//////                }
////                } catch (FileNotFoundException e) {
////                    e.printStackTrace();
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////                // Flushes the stream
////                stream.flush();
////                // Closes the stream
////                stream.close();
////            } catch (IOException e) // Catch the exception
////            {
////                e.printStackTrace();
////            }
////            // Parse the gallery image url to uri
////            Uri savedImageURI = Uri.parse(file.getAbsolutePath());
////            // Return the saved image Uri
////            return savedImageURI;
////        }
//
//        protected Uri saveAudioToInternalStorage(InputStream input, String name) {
//            // Initialize ContextWrapper
//            ContextWrapper wrapper = new ContextWrapper(getApplicationContext());
//            // Initializing a new file
//            // The bellow line return a directory in internal storage
//            File file = wrapper.getDir("Audios", MODE_PRIVATE);
//            // Create a file to save the image
//            file = new File(file, name + ".mp3");
//            try {
//                // Initialize a new OutputStream
//                OutputStream stream = null;
//                // If the output file exists, it can be replaced or appended to it
//                stream = new FileOutputStream(file);
//                // Compress the bitmap
//                try {
//                    try (OutputStream output = new FileOutputStream(file)) {
//                        byte[] buffer = new byte[4 * 1024];
//                        // or other buffer size
//                        int read;
//                        while ((read = input.read(buffer)) != -1) {
//                            output.write(buffer, 0, read);
//                        }
//                        output.flush();
//                    }
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                // Flushes the stream
//                stream.flush();
//                // Closes the stream
//                stream.close();
//            } catch (IOException e) // Catch the exception
//            {
//                e.printStackTrace();
//            }
//            // Parse the gallery image url to uri
//            Uri savedImageURI = Uri.parse(file.getAbsolutePath());
//            // Return the saved image Uri
//            return savedImageURI;
//        }
//
//        private void getConatctsApi() {
//            ApiHelper.getInstance().getAllContactsListApi(this, this);
//        }
//
//        @Override
//        public void onGetAllContactsSuccess(GetAllContactResp getAllContactResp) {
//            for (int i = 0; i < getAllContactResp.getContacts().size(); i++)
//                App.getInstance().getDB().scheduleDao().insertContactDetails(getAllContactResp.getContacts().get(i));
//            Intent intent = new Intent(this, GetCallLogsDetails.class);
//            // add infos for the service which file to download and where to store
//            intent.putExtra(AppConstants.SetCallLogs, "");
//            startService(intent);
//        }
//
//        @Override
//        public void onGetAllContactsFailure() {
//
//        }
//
//        @Override
//        protected void onResume() {
//            super.onResume();
//            Window window = getWindow();
//            window.setStatusBarColor(getResources().getColor(R.color.darkblue));
//
//            int permissionCallPhone1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG);
//            if (permissionCallPhone1 != PackageManager.PERMISSION_GRANTED) {
//                new GrantingPermission().askForPermission(this, Manifest.permission.READ_CALL_LOG, AppConstants.READ_CALL_LOG);
//            } else {
//            }
//
//        }
//
//
//        public void onFragmentChangeListener(int fragmentId, boolean addToBackstack, Bundle bundle) {
//            Fragment fragment = null;
//            String fragmentName = null;
//            switch (fragmentId) {
//                case FragmentId.CONTACTFRAGMENT:
//                    tvFragmentName.setText(AppConstants.contacts);
//                    fragment = new ContactFragment();
//                    contactFragment = (ContactFragment) fragment;
//                    fragmentName = ContactFragment.class.getSimpleName();
//                    break;
//                case FragmentId.CALLLOGFRAGMENT:
//                    tvFragmentName.setText(AppConstants.recents);
//                    fragment = new CallLogFragment();
//                    fragmentName = CallLogFragment.class.getSimpleName();
//                    break;
//
//            }
//            if (fragment != null) {
//                fragment.setArguments(bundle);
//                changeFragment(fragment, addToBackstack, fragmentName);
//            }
//        }
//
//        private void changeFragment(Fragment fragment, boolean addToBackstack, String fragmentName) {
//
//            fragmentManager = getSupportFragmentManager();
//            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.ll_container_layout, fragment, fragmentName);
//            if (addToBackstack)
//                transaction.addToBackStack(fragmentName);
//            transaction.commit();
//        }
//
//        @OnClick({R.id.ll_contact, R.id.ll_calllog})
//        public void onViewClicked(View view) {
//            switch (view.getId()) {
//                case R.id.ll_contact:
//                    onFragmentChangeListener(FragmentId.CONTACTFRAGMENT, false, null);
//                    break;
//                case R.id.ll_calllog:
//                    onFragmentChangeListener(FragmentId.CALLLOGFRAGMENT, false, null);
//                    break;
//            }
//        }
//
//
//        @Override
//        public void onReadyForSpeech(Bundle params) {
//
//        }
//
//        @Override
//        public void onBeginningOfSpeech() {
//
//        }
//
//        @Override
//        public void onRmsChanged(float rmsdB) {
//
//        }
//
//        @Override
//        public void onBufferReceived(byte[] buffer) {
//            Log.e("", "" + buffer);
//        }
//
//        @Override
//        public void onEndOfSpeech() {
//
//        }
//
//        @Override
//        public void onError(int error) {
//
//        }
//
//        @Override
//        public void onResults(Bundle results) {
//
//        }
//
//        @Override
//        public void onPartialResults(Bundle partialResults) {
//
//        }
//
//        @Override
//        public void onEvent(int eventType, Bundle params) {
//
//        }
//    }
//
//
//
//
//}
