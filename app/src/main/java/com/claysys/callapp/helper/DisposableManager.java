package com.claysys.callapp.helper;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Susan Sajeev on 01-12-2017.
 */

public class DisposableManager {
    private static CompositeDisposable compositeDisposable;
    public DisposableManager() {
    }
    public static void add(Disposable disposable){
        getCompositeDisposable().add(disposable);

    }public static void dispose(){
        getCompositeDisposable().clear();
        getCompositeDisposable().dispose();
    }


    public static CompositeDisposable getCompositeDisposable() {
        if(compositeDisposable ==null || compositeDisposable.isDisposed()){
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }
}
