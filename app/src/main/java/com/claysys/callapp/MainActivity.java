package com.claysys.callapp;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.fragment.CallLogFragment;
import com.claysys.callapp.fragment.ChatListFragment;
import com.claysys.callapp.fragment.ContactFragment;
import com.claysys.callapp.fragment.MyTasksFragment;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.response.GetAllContactResp;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.sqllite.helper.EmployeeTb;
import com.claysys.callapp.utlis.AppConstants;
import com.claysys.callapp.utlis.GrantingPermission;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements IResponseView, RecognitionListener {

    @BindView(R.id.ll_container_layout)
    FrameLayout llContainerLayout;
    @BindView(R.id.im_contact)
    ImageView imContact;
    @BindView(R.id.ll_contact)
    LinearLayout llContact;
    @BindView(R.id.im_calllog)
    ImageView imCalllog;
    @BindView(R.id.ll_calllog)
    LinearLayout llCalllog;
    @BindView(R.id.ll_bottom_navigation)
    LinearLayout llBottomNavigation;
    @BindView(R.id.top_parent)
    RelativeLayout topParent;
    @BindView(R.id.iconMain)
    ImageView iconMain;
    @BindView(R.id.tv_fragment_name)
    TextView tvFragmentName;
    @BindView(R.id.header)
    LinearLayout header;

    @BindView(R.id.im_chat)
    ImageView imchat;
    @BindView(R.id.ll_chat)
    LinearLayout llchat;
    @BindView(R.id.im_task)
    ImageView imtask;
    @BindView(R.id.ll_task)
    LinearLayout lltask;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    FirebaseFirestore db;
    @BindView(R.id.menu_toolbar)
    ImageView menuToolbar;
    @BindView(R.id.add_toolbar)
    ImageView addToolbar;
    @BindView(R.id.search_tv)
    EditText searchTv;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    @BindView(R.id.tv_recents)
    TextView tvRecents;
    @BindView(R.id.tv_Contacts)
    TextView tvContacts;
    @BindView(R.id.tv_chat)
    TextView tvChat;
    private FragmentManager fragmentManager;
    ContactFragment contactFragment;
    CallLogFragment callLogFragment;
    ChatListFragment chatListFragment;
    MyTasksFragment myTasksFragment;
    CallLogTb mainCallLog;


    ArrayList<ContactTb> contactTbsUsers;

    SharedPreferredManager sharedPreferredManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        sharedPreferredManager = new SharedPreferredManager(this);
        getConatctsApi();
        getAllEmployeeApi();

        fragmentManager = getSupportFragmentManager();
        onFragmentChangeListener(FragmentId.CONTACTFRAGMENT, false, null);
        int permissionCallPhone12 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        if (permissionCallPhone12 != PackageManager.PERMISSION_GRANTED) {
            new GrantingPermission().askForPermission(this, Manifest.permission.READ_CONTACTS, AppConstants.READ_CONTACTS);
        }

//        Intent intent = new Intent(this, GetCallLogsDetails.class);
//        // add infos for the service which file to download and where to store
//        startService(intent);


        db = FirebaseFirestore.getInstance();
        searchTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                if (arg0.length() == 0) {
                    contactFragment.setFilterCallToAdapter(null);
                } else {
                    String text = searchTv.getText().toString().toLowerCase(Locale.getDefault());
                    contactFragment.setFilterCallToAdapter(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getAllEmployeeApi() {
        ApiHelper.getInstance().getAllEmployeees(this, this, "");
    }

    @Override
    public void onFetchAllEmplyeeSucess(ArrayList<EmployeeTb> employeeTbsList) {
        for (int i = 0; i < employeeTbsList.size(); i++)
            App.getInstance().getDB().scheduleDao().insertEmployeeDetails(employeeTbsList.get(i));
    }

    @Override
    public void onFetchAllEmplyeeFailure() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Bundle bundle = data.getExtras();

            // This part works fine.
            ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
            String text = "";
//            for (String result : matches)
//                text += result + "\n";
            text += matches.get(0);
            if (callLogFragment != null) {
                callLogFragment.setSpeechToTextView(text);
                if(mainCallLog.getMsg()!=null)
                    mainCallLog.setMsg(mainCallLog.getMsg() + " " + text);
                else
                    mainCallLog.setMsg(text);

            }
            // But audioUri is always null !!!
            Uri audioUri = data.getData();


            Log.e("wwww", " " + audioUri.getPath());
            ContentResolver contentResolver = getContentResolver();
            InputStream filestream = null;
            try {
                filestream = contentResolver.openInputStream(audioUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            CallLogTb callLogTbFromDb = App.getInstance().getDB().scheduleDao().getMsg(mainCallLog.getId());
            if (callLogTbFromDb.getAmrFileUrl() != null) {
                Calendar rightNow = Calendar.getInstance();
                Uri audioLocalUri = saveAudioToInternalStorage(filestream, rightNow.getTimeInMillis() + "ToMerger", true);

                streamMethod(new File(callLogTbFromDb.getAmrFileUrl()), audioLocalUri);
            } else {
                InputStream filestream2 = null;
                try {
                    filestream2 = contentResolver.openInputStream(audioUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Uri audioLocalUri = saveAudioToInternalStorage(filestream, mainCallLog.getDate() + mainCallLog.getId() + "First", true);
                Uri audioLocalUriMp3 = saveAudioToInternalStorage(filestream2, mainCallLog.getDate() + mainCallLog.getId() + "FirstMp3", false);
                long id = App.getInstance().getDB().scheduleDao().updateAmrAudioUrl(audioLocalUri.getPath(), mainCallLog.getId());
                long idMp3 = App.getInstance().getDB().scheduleDao().updateAudioUrl(audioLocalUriMp3.getPath(), mainCallLog.getId());
                Log.e("" +idMp3, "" + id);
            }
        } else {
            callLogFragment.setSpeechToMediaPlayer(null, null, false);
        }

    }

    private void streamMethod(File audiUri, Uri mainUri) {
        File audioUriFile = audiUri;
        File mainUriFile = new File(mainUri.getPath());
        try {
            FileInputStream inputStream1 = new FileInputStream(audioUriFile.getAbsolutePath());
            FileInputStream inputStream2 = new FileInputStream(mainUriFile.getAbsolutePath());

            FileOutputStream outputStream = new FileOutputStream(new File(mainUriFile.getAbsolutePath() + "w"));

            int temp = 0;
            inputStream2.read();
            inputStream2.read();
            inputStream2.read();
            inputStream2.read();
            inputStream2.read();
            inputStream2.read();

            while ((temp = inputStream2.read()) != -1) {
                outputStream.write(temp);
            }
            outputStream.close();
            inputStream2.close();
            inputStream2 = new FileInputStream(mainUriFile.getAbsolutePath() + "w");

            SequenceInputStream inputStream3 = new SequenceInputStream(inputStream1, inputStream2);
            outputStream = new FileOutputStream(mainUriFile);
            //FileWriter fileWriter = new FileWriter(soundFile, true);
            while ((temp = inputStream3.read()) != -1) {
                outputStream.write(temp);
            }
            outputStream.close();
            inputStream1.close();
            inputStream2.close();
            inputStream3.close();
            //fileWriter.close();
            //fileWriter.flush();
            long id = App.getInstance().getDB().scheduleDao().updateAmrAudioUrl(mainUriFile.getPath(), mainCallLog.getId());
            FileInputStream inputStreamAfterMerge = new FileInputStream(mainUriFile.getPath());
            saveAudioToInternalStorage(inputStreamAfterMerge, mainCallLog.getDate() + "UrlAfterMerge", false);
        } catch (Exception e) {
            Log.e("", "" + e);
        }

    }

    protected Uri saveAudioToInternalStorage(InputStream input, String name, boolean isAmr) {
        // Initialize ContextWrapper
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());
        // Initializing a new file
        // The bellow line return a directory in internal storage
        File file = wrapper.getDir("Audios", MODE_PRIVATE);
        // Create a file to save the image
        if (isAmr)
            file = new File(file, name + ".amr");
        else
            file = new File(file, name + ".mp3");
        try {
            // Initialize a new OutputStream
            OutputStream stream = null;
            // If the output file exists, it can be replaced or appended to it
            stream = new FileOutputStream(file);
            // Compress the bitmap
            try {
                try (OutputStream output = new FileOutputStream(file)) {
                    byte[] buffer = new byte[4 * 1024];
                    // or other buffer size
                    int read;
                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                    output.flush();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Flushes the stream
            stream.flush();
            // Closes the stream
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!isAmr) {
            long id = App.getInstance().getDB().scheduleDao().updateAudioUrl(file.getPath(), mainCallLog.getId());
            Log.e("",""+id);
//            saveAudioFileApi(file);
        }
        // Parse the gallery image url to uri
        Uri savedImageURI = Uri.parse(file.getAbsolutePath());
        callLogFragment.setSpeechToMediaPlayer(file, savedImageURI, true);
        // Return the saved image Uri
        return savedImageURI;
    }


    private void getConatctsApi() {
        ApiHelper.getInstance().getAllContactsListApi(this, this);
    }

    @Override
    public void onGetAllContactsSuccess(GetAllContactResp getAllContactResp) {
        for (int i = 0; i < getAllContactResp.getContacts().size(); i++) {
            String lastName = getAllContactResp.getContacts().get(i).getLastName();
            lastName  = lastName.replace("\r", "");
            lastName = lastName.replace("\n", "");
            getAllContactResp.getContacts().get(i).setLastName(lastName);
            String mobile = getAllContactResp.getContacts().get(i).getMobile();
            mobile  = mobile.replace("\r", "");
            mobile = mobile.replace("\n", "");
            getAllContactResp.getContacts().get(i).setMobile(mobile);
            App.getInstance().getDB().scheduleDao().insertContactDetails(getAllContactResp.getContacts().get(i));

        }
    }

    @Override
    public void onGetAllContactsFailure() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.darkblue));

        int permissionCallPhone1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG);
        if (permissionCallPhone1 != PackageManager.PERMISSION_GRANTED) {
            new GrantingPermission().askForPermission(this, Manifest.permission.READ_CALL_LOG, AppConstants.READ_CALL_LOG);
        } else {
        }

    }


    public void onFragmentChangeListener(int fragmentId, boolean addToBackstack, Bundle bundle) {
        Fragment fragment = null;
        String fragmentName = null;
        switch (fragmentId) {
            case FragmentId.CONTACTFRAGMENT:
                tvChat.setTextColor(getResources().getColor(R.color.grayColorFont));
                tvContacts.setTextColor(getResources().getColor(R.color.blue));
                tvRecents.setTextColor(getResources().getColor(R.color.grayColorFont));
                addToolbar.setVisibility(View.INVISIBLE);
                tvFragmentName.setText(AppConstants.contacts);
                llSearch.setVisibility(View.VISIBLE);
                fragment = new ContactFragment();
                contactFragment = (ContactFragment) fragment;
                fragmentName = ContactFragment.class.getSimpleName();
                imContact.setImageResource(R.drawable.contact_blue);
                imCalllog.setImageResource(R.drawable.recent);
                imchat.setImageResource(R.drawable.chat);
                imtask.setImageResource(R.drawable.task);
                break;
            case FragmentId.CALLLOGFRAGMENT:
                tvChat.setTextColor(getResources().getColor(R.color.grayColorFont));
                tvContacts.setTextColor(getResources().getColor(R.color.grayColorFont));
                tvRecents.setTextColor(getResources().getColor(R.color.blue));
                addToolbar.setVisibility(View.INVISIBLE);
                tvFragmentName.setText(AppConstants.recents);
                llSearch.setVisibility(View.GONE);
                fragment = new CallLogFragment();
                callLogFragment = (CallLogFragment) fragment;
                fragmentName = CallLogFragment.class.getSimpleName();
                imContact.setImageResource(R.drawable.contact);
                imCalllog.setImageResource(R.drawable.recent_blue);
                imchat.setImageResource(R.drawable.chat);
                imtask.setImageResource(R.drawable.task);
                break;
            case FragmentId.CHATLISTFRAGMENT:
                tvChat.setTextColor(getResources().getColor(R.color.blue));
                tvContacts.setTextColor(getResources().getColor(R.color.grayColorFont));
                tvRecents.setTextColor(getResources().getColor(R.color.grayColorFont));
                addToolbar.setVisibility(View.VISIBLE);
                tvFragmentName.setText(AppConstants.ChatLists);
                llSearch.setVisibility(View.GONE);
                fragment = new ChatListFragment();
                chatListFragment = (ChatListFragment) fragment;
                fragmentName = ChatListFragment.class.getSimpleName();
                imContact.setImageResource(R.drawable.contact);
                imCalllog.setImageResource(R.drawable.recent);
                imchat.setImageResource(R.drawable.chat_blue);
                imtask.setImageResource(R.drawable.task);
                break;
            case FragmentId.MYTASKFRAGMENT:
                addToolbar.setVisibility(View.INVISIBLE);
                tvFragmentName.setText(AppConstants.MYTASKS);
                llSearch.setVisibility(View.GONE);
                fragment = new MyTasksFragment();
                myTasksFragment = (MyTasksFragment) fragment;
                fragmentName = MyTasksFragment.class.getSimpleName();
                imContact.setImageResource(R.drawable.contact);
                imCalllog.setImageResource(R.drawable.recent);
                imchat.setImageResource(R.drawable.chat);
                imtask.setImageResource(R.drawable.task_blue);
                break;

        }
        if (fragment != null) {
            fragment.setArguments(bundle);
            changeFragment(fragment, addToBackstack, fragmentName);
        }
    }


    private void changeFragment(Fragment fragment, boolean addToBackstack, String fragmentName) {

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.ll_container_layout, fragment, fragmentName);
        if (addToBackstack)
            transaction.addToBackStack(fragmentName);
        transaction.commit();
    }

    @OnClick({R.id.ll_contact, R.id.search_tv, R.id.ll_calllog, R.id.add_toolbar, R.id.ll_chat, R.id.im_task})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_contact:
                if(tvContacts.getCurrentTextColor()!=getResources().getColor(R.color.blue))
                    onFragmentChangeListener(FragmentId.CONTACTFRAGMENT, false, null);
                break;
            case R.id.ll_calllog:
                if(tvRecents.getCurrentTextColor()!=getResources().getColor(R.color.blue))
                    onFragmentChangeListener(FragmentId.CALLLOGFRAGMENT, false, null);
                break;
            case R.id.ll_chat:
                if(tvChat.getCurrentTextColor()!=getResources().getColor(R.color.blue))
                    onFragmentChangeListener(FragmentId.CHATLISTFRAGMENT, false, null);
                break;
            case R.id.im_task:
                onFragmentChangeListener(FragmentId.MYTASKFRAGMENT, false, null);
                break;
            case R.id.add_toolbar:
                chatListFragment.setUsersListAdapter();
                break;
            case R.id.search_tv:
                break;
        }
    }


    @Override
    public void onReadyForSpeech(Bundle params) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.e("", "" + buffer);
    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int error) {

    }

    @Override
    public void onResults(Bundle results) {

    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }


    public void setCallogLogList(CallLogTb callLogTb) {
        this.mainCallLog = callLogTb;

    }


}
