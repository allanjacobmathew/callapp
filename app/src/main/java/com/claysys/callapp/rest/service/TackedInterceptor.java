package com.claysys.callapp.rest.service;

import android.content.Context;

import com.claysys.callapp.rest.response.TokenResponse;
import com.claysys.callapp.utlis.AppConstants;
import com.claysys.callapp.utlis.TokenManager;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by muhammed on 9/27/2017.
 */

public class TackedInterceptor implements Interceptor {
    private Context context;

    public TackedInterceptor(Context context) {
        this.context = context;
    }

    private String bodyToString(Response response) throws IOException {

        return response.body().string().toString();
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request request = chain.request();

        //Build new request
        Request.Builder builder = request.newBuilder();
        builder.header("Accept", "application/json"); //if necessary, say to consume JSON
        TokenManager instance = TokenManager.getInstance(context);
//        if (instance.hasToken()) {
//            return makeOriginalRequest(chain, builder, request);
//        } else {
//            return getToken(builder, chain, request);
//        }
        if (instance.getToken()==null ||instance.getToken().equals("")|| instance.getToken().equals(null)) {
            return getToken(builder, chain, request);
        } else {
            return makeOriginalRequest(chain, builder, request);
        }

    }

    private Response getToken(Request.Builder builder, Chain chain, Request request) throws IOException {
        TokenManager instance = TokenManager.getInstance(context);
        RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"), "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"deviceID\"\r\n\r\n" + instance.getDeviceId() + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
        Request build = builder.url(AppConstants.BASE_URL + "GetToken").post(body).build();
        Response tokenResponse = chain.proceed(build);
        if (tokenResponse.isSuccessful()) {
            String jspnResponse = tokenResponse.body().string();
            Gson gson = new Gson();
            TokenResponse responseRerfer = gson.fromJson(jspnResponse, TokenResponse.class);
            instance.setToken(responseRerfer.getAccessToken());
            instance.setRefreshToken(responseRerfer.getRefreshToken());
            return makeOriginalRequest(chain, builder, request);
        }
        return null;
    }

    private Response makeOriginalRequest(Chain chain, Request.Builder builder, Request request) throws IOException {
        TokenManager instance = TokenManager.getInstance(context);
        String token = instance.getToken(); //save token of this request for future

        builder.post(request.body());
        builder.url(request.url());
        setAuthHeader(builder, token); //write current token to request

//        Request requestNew = builder.build(); //overwrite old request

        Response response = chain.proceed(builder.build()); //perform request, here original request will be executed

        if (response.code() == 401) {

            if (instance.hasRefreshToken()) {
                RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"), "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"deviceID\"\r\n\r\n" + instance.getDeviceId() + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"refreshToken\"\r\n\r\n" + instance.getRefreshToken() + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
                Request build = builder.url(AppConstants.BASE_URL  + "RefreshToken").post(body).build();
                Response tokenResponse = chain.proceed(build);
                if (tokenResponse.isSuccessful()) {
                    String jspnResponse = tokenResponse.body().string();
                    Gson gson = new Gson();
                    TokenResponse responseRerfer = gson.fromJson(jspnResponse, TokenResponse.class);
                    instance.setToken(responseRerfer.getAccessToken());
                    instance.setRefreshToken(responseRerfer.getRefreshToken());
//                    response = makeOriginalRequest(chain, builder, request);
                }
            } else {
                return getToken(builder, chain, request);
            }


        }
        return response;
    }

    private void setAuthHeader(Request.Builder builder, String token) {
        if (token != null) //Add Auth token to each request if authorized
            builder.header("authorization", String.format("Bearer %s", token));
    }


}
