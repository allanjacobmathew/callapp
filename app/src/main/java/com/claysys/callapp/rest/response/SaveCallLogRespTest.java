package com.claysys.callapp.rest.response;

import com.google.gson.annotations.SerializedName;

public class SaveCallLogRespTest {

    @SerializedName("fileUrl")
    String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @SerializedName("status")
    String status;
}
