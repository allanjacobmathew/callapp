package com.claysys.callapp.rest.service;

import com.claysys.callapp.rest.request.LoginApiReq;
import com.claysys.callapp.rest.request.SaveCallLogNoteReq;
import com.claysys.callapp.rest.request.SaveCallLogReq;
import com.claysys.callapp.rest.request.SaveMsgDetails;
import com.claysys.callapp.rest.response.GetAllContactResp;
import com.claysys.callapp.rest.response.LoginResp;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.rest.response.SaveCallLogRespTest;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.sqllite.helper.EmployeeTb;

import java.util.ArrayList;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Susan on 17-10-2018.
 */

public interface ApiInterface {
    @GET("Contacts/GetAllContacts")
    Observable<GetAllContactResp> getAllContacts(@Header("Authorization") String acessToken);


    @POST("Log/LogIt")
    Observable<SaveCallLogResp> saveCallLogs(@Header("Authorization") String acessToken, @Body SaveCallLogReq saveCallLogReq);

    @POST("AudioDetails/LogNotesAPI")
    Observable<SaveCallLogResp> saveCallLogNotes(@Header("Authorization") String acessToken, @Body SaveCallLogNoteReq saveCallLogNoteReq);

//    @Multipart
//    @POST("AudioUpload")
//    Observable<SaveCallLogResp> saveAudioUpload(@Part MultipartBody.Part file);

    @Multipart
    @POST("AudioUpload/Post")
    Observable<Response<ResponseBody>> saveAudioUpload(@Header("Authorization") String acessToken, @Part MultipartBody.Part file);

    @POST("Employee/AddEmployee")
    Observable<SaveCallLogResp> addEmployee(@Header("Authorization") String acessToken, @Body ContactTb contactTb);

    @POST("Message/MessageStart")
    Observable<SaveCallLogResp> sendStartMsg(@Header("Authorization") String acessToken, @Body SaveMsgDetails saveMsgDetails);

    @POST("Message/MessageEnd")
    Observable<SaveCallLogResp> sendStoptMsg(@Header("Authorization") String acessToken, @Body SaveMsgDetails saveMsgDetails);

    @POST("Employee/FetchAllEmployees")
    Observable<ArrayList<EmployeeTb>> fetchAllEmployees(@Header("Authorization") String acessToken, @Body String saveMsgDetails);

    @POST("EmpAuth/Authenticate")
    Observable<LoginResp> login(@Body LoginApiReq loginApiReq);
}
