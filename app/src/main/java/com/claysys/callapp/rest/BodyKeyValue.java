package com.claysys.callapp.rest;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by muhammed on 10/3/2017.
 */

public class BodyKeyValue {
    MediaType type = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");

    private Map<String, RequestBody> bodyMap;

    public BodyKeyValue() {
        bodyMap = new HashMap<>();
    }

    private Map<String, RequestBody> getBodyMap() {
        return bodyMap;
    }

    public void put(String key, String value) {
        RequestBody requestBody = RequestBody.create(type, value);
        getBodyMap().put(key, requestBody);
    }

    public RequestBody get(String key) {
        return getBodyMap().get(key);
    }

    public Map<String, RequestBody> build() {
        return bodyMap;
    }
}
