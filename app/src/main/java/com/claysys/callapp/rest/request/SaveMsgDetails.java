package com.claysys.callapp.rest.request;

public class SaveMsgDetails {

    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String firstEmployeeId;
    public String firstEmployeeFIRId;
    public String secondEmployeeId;
    public String secondEmployeeFIRId;
    public String taggedMemberId;
    public String taggedMemberGUID;
    public String channelFIRId;
    public String startMessageFIRId;


    public String endMessageFIRId;


    public String getFirstEmployeeId() {
        return firstEmployeeId;
    }

    public void setFirstEmployeeId(String firstEmployeeId) {
        this.firstEmployeeId = firstEmployeeId;
    }

    public String getFirstEmployeeFIRId() {
        return firstEmployeeFIRId;
    }

    public void setFirstEmployeeFIRId(String firstEmployeeFIRId) {
        this.firstEmployeeFIRId = firstEmployeeFIRId;
    }

    public String getSecondEmployeeId() {
        return secondEmployeeId;
    }

    public void setSecondEmployeeId(String secondEmployeeId) {
        this.secondEmployeeId = secondEmployeeId;
    }

    public String getSecondEmployeeFIRId() {
        return secondEmployeeFIRId;
    }

    public void setSecondEmployeeFIRId(String secondEmployeeFIRId) {
        this.secondEmployeeFIRId = secondEmployeeFIRId;
    }

    public String getTaggedMemberId() {
        return taggedMemberId;
    }

    public void setTaggedMemberId(String taggedMemberId) {
        this.taggedMemberId = taggedMemberId;
    }

    public String getTaggedMemberGUID() {
        return taggedMemberGUID;
    }

    public void setTaggedMemberGUID(String taggedMemberGUID) {
        this.taggedMemberGUID = taggedMemberGUID;
    }

    public String getChannelFIRId() {
        return channelFIRId;
    }

    public void setChannelFIRId(String channelFIRId) {
        this.channelFIRId = channelFIRId;
    }

    public String getStartMessageFIRId() {
        return startMessageFIRId;
    }

    public void setStartMessageFIRId(String startMessageFIRId) {
        this.startMessageFIRId = startMessageFIRId;
    }

    public String getEndMessageFIRId() {
        return endMessageFIRId;
    }

    public void setEndMessageFIRId(String endMessageFIRId) {
        this.endMessageFIRId = endMessageFIRId;
    }
}
