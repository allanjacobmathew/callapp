package com.claysys.callapp.rest.responsehandler;

import android.content.Context;
import android.util.Log;

import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.utlis.AppConstants;
import com.google.gson.JsonObject;

import java.io.IOException;

import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by susan sajeev on 22-10-2018.
 */

public class SaveCallLogRespHandlFile extends DisposableObserver<Response<ResponseBody>>{
    Context context;
    IResponseView iRespView;
    int apiId;

    public SaveCallLogRespHandlFile(Context context, IResponseView iRespView, int apiId) {
        this.context = context;
        this.iRespView = iRespView;
        this.apiId = apiId;
    }

    public SaveCallLogRespHandlFile() {

    }

    @Override
    public void onNext(Response<ResponseBody> value) {
        String statusStr[] = new String[0];
        String fileUrlStr[] = new String[0];
        Log.e("UserDetdLandler","333333"+value);
        try {
            String responseRecieved = value.body().string();
            int index  =responseRecieved.indexOf("--MultipartDataMediaFormatterBoundary1q2w3e");
            String statusStart = responseRecieved.substring(responseRecieved.lastIndexOf("status"));
            String status =  statusStart.substring(0,statusStart.indexOf("--MultipartDataMediaFormatterBoundary"));
            String fileUrlStart = responseRecieved.substring(responseRecieved.lastIndexOf("fileUrl"));
            String fileUrl = fileUrlStart.substring(0,fileUrlStart.indexOf("--MultipartDataMediaFormatterBoundary"));
            Log.e("gd","rgfd"+responseRecieved);

            statusStr = status.split("\\r\\n\\r\\n");
            fileUrlStr = fileUrl.split("\\r\\n\\r\\n");
            Log.e("gd","rgfd"+responseRecieved);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String statusStrValue = statusStr[1].replaceAll("\\r\\n","");
        if(statusStrValue.equals(AppConstants.Success)) {
            String fileOrgUrl = fileUrlStr[1].replaceAll("\\r\\n", "");
            iRespView.onSaveCallLogsAudioFileSucess("", fileOrgUrl);
        }

    }

    @Override
    public void onError(Throwable e) {
        Log.e("something=====","333333=========="+e);
        iRespView.onSaveCallLogsAudioFileFailureTest();
        iRespView.dismissLoader();
    }

    @Override
    public void onComplete() {
        iRespView.dismissLoader();
    }
}
