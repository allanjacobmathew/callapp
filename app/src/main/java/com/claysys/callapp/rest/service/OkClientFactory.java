package com.claysys.callapp.rest.service;

import android.app.Application;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 *
 */
public class OkClientFactory {
    // Cache size for the OkHttpClient

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    private OkClientFactory() {
    }
    @NonNull
    public static OkHttpClient provideOkHttpClient(Application app) {
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        OkHttpClient.Builder builder = new OkHttpClient.Builder().readTimeout(25, TimeUnit.SECONDS).connectTimeout(25, TimeUnit.SECONDS)
                .cache(cache);
//        OkHttpClient.Builder builder = new OkHttpClient.Builder()
//                .cache(cache);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        TackedInterceptor tockenIntercepter = new TackedInterceptor(app.getApplicationContext());

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        builder.interceptors().add(tockenIntercepter);
        builder.interceptors().add(loggingInterceptor);

        return builder.build();
    }
}
