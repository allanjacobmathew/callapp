package com.claysys.callapp.rest.response;

import com.google.gson.annotations.SerializedName;

public class SaveCallLogResp {
    int id;
    @SerializedName("fileUrl")
    String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @SerializedName("status")
    String status;
}
