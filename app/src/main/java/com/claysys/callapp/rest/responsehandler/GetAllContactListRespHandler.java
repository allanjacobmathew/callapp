package com.claysys.callapp.rest.responsehandler;

import android.content.Context;
import android.util.Log;

import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.response.GetAllContactResp;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by susan sajeev on 22-10-2018.
 */

public class GetAllContactListRespHandler extends DisposableObserver<GetAllContactResp>{
    Context context;
    IResponseView iRespView;
    int apiId;

    public GetAllContactListRespHandler(Context context, IResponseView iRespView, int apiId) {
        this.context = context;
        this.iRespView = iRespView;
        this.apiId = apiId;
    }

    public GetAllContactListRespHandler() {
    }

    @Override
    public void onNext(GetAllContactResp value) {
        Log.e("UserDetdLandler","333333"+value);
        iRespView.onGetAllContactsSuccess(value);
    }

    @Override
    public void onError(Throwable e) {
        Log.e("something=====","333333=========="+e);
        iRespView.onGetAllContactsFailure();
        iRespView.dismissLoader();
    }

    @Override
    public void onComplete() {
        iRespView.dismissLoader();
    }
}
