package com.claysys.callapp.rest.service;


import com.claysys.callapp.App;
import com.claysys.callapp.utlis.AppConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by susan sajeev on 22-10-2018
 */

public class ApiClient {
    private Retrofit retrofit;
    private Retrofit retrofitForGetPurchaseDetails;

    public ApiClient() {
        OkHttpClient httpClient;

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setLenient()
                .setPrettyPrinting()
                .create();



        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
       // if(BuildConfig.DEBUG){
            httpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

//        }else{
//            httpClient = new OkHttpClient.Builder().build();
//        }
        //creating an instance of recrofit
        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(App.getInstance().getOkHttpClient())
//
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();


    }
    public Retrofit getRetrofit(){
        return retrofit;
    }

}
