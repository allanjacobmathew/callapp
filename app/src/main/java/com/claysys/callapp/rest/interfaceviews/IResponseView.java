package com.claysys.callapp.rest.interfaceviews;


import com.claysys.callapp.rest.response.GetAllContactResp;
import com.claysys.callapp.rest.response.LoginResp;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.sqllite.helper.EmployeeTb;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by susan sajeev on 22-10-2018.
 */

public interface IResponseView {
//    For Login
    default void onGetAllContactsSuccess(GetAllContactResp getAllContactResp) {}
    default void onGetAllContactsFailure() {}

    default void onSaveCallLogsSucess(SaveCallLogResp saveCallLogResp) {}
    default void onSaveCallLogsFailure() {}


    default void onSaveCallLogsAudioFileSucess(String status, String fileUrl){}
    default void onSaveCallLogsAudioFileFailureTest() {}

    default void onFetchAllEmplyeeSucess(ArrayList<EmployeeTb> employeeTbsList){}
    default void onFetchAllEmplyeeFailure() {}

    default void onLoginApiSucess(LoginResp loginResp){}
    default void onLoginApiFailure() {}

    default void dismissLoader() {}

}
