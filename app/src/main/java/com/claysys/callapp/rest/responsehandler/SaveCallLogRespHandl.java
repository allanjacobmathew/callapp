package com.claysys.callapp.rest.responsehandler;

import android.content.Context;
import android.util.Log;

import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.response.GetAllContactResp;
import com.claysys.callapp.rest.response.SaveCallLogResp;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by susan sajeev on 22-10-2018.
 */

public class SaveCallLogRespHandl extends DisposableObserver<SaveCallLogResp>{
    Context context;
    IResponseView iRespView;
    int apiId;

    public SaveCallLogRespHandl(Context context, IResponseView iRespView, int apiId) {
        this.context = context;
        this.iRespView = iRespView;
        this.apiId = apiId;
    }

    public SaveCallLogRespHandl() {
    }

    @Override
    public void onNext(SaveCallLogResp value) {
        Log.e("UserDetdLandler","333333"+value);
        iRespView.onSaveCallLogsSucess(value);
    }

    @Override
    public void onError(Throwable e) {
        Log.e("something=====","333333=========="+e);
        iRespView.onSaveCallLogsFailure();
        iRespView.dismissLoader();
    }

    @Override
    public void onComplete() {
        iRespView.dismissLoader();
    }
}
