package com.claysys.callapp.rest.response;

import com.claysys.callapp.sqllite.helper.ContactTb;

import java.util.ArrayList;

public class GetAllContactResp {
    ArrayList<ContactTb> contacts;

    public ArrayList<ContactTb> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<ContactTb> contacts) {
        this.contacts = contacts;
    }
}
