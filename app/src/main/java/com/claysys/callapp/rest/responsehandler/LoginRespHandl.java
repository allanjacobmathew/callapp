package com.claysys.callapp.rest.responsehandler;

import android.content.Context;
import android.util.Log;

import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.response.LoginResp;
import com.claysys.callapp.sqllite.helper.EmployeeTb;

import java.util.ArrayList;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by susan sajeev on 22-10-2018.
 */

public class LoginRespHandl extends DisposableObserver<LoginResp>{
    Context context;
    IResponseView iRespView;
    int apiId;

    public LoginRespHandl(Context context, IResponseView iRespView, int apiId) {
        this.context = context;
        this.iRespView = iRespView;
        this.apiId = apiId;
    }

    public LoginRespHandl() {
    }

    @Override
    public void onNext(LoginResp value) {
        Log.e("UserDetdLandler","333333"+value);
        iRespView.onLoginApiSucess(value);
    }

    @Override
    public void onError(Throwable e) {
        Log.e("something=====","333333=========="+e);
        iRespView.onLoginApiFailure();
        iRespView.dismissLoader();
    }

    @Override
    public void onComplete() {
        iRespView.dismissLoader();
    }
}
