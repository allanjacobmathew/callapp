package com.claysys.callapp;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyTaskDetailActivity extends AppCompatActivity {

    @BindView(R.id.menu_toolbar)
    ImageView menuToolbar;
    @BindView(R.id.iconMain)
    ImageView iconMain;
    @BindView(R.id.add_toolbar)
    ImageView addToolbar;
    @BindView(R.id.tv_fragment_name)
    TextView tvFragmentName;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_task_detail);
        ButterKnife.bind(this);
        header.setVisibility(View.GONE);
        menuToolbar.setVisibility(View.INVISIBLE);
        addToolbar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.darkblue));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
