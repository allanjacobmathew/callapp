package com.claysys.callapp.dialogs;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.ChatActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.adapter.ChatUsersListAdapter;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.rey.material.app.BottomSheetDialog;

import java.util.ArrayList;

public class SelectMeUserDialog implements ChatUsersListAdapter.OnClickChatListUser {

    Context c;

    SharedPreferredManager sharedPreferredManager;
    BottomSheetDialog mDialogAddStatus;

    public void openUpDialogToAdd(Context c , ArrayList<ContactTb> contactTbArrayList) {
        this.c =c;
        mDialogAddStatus = new BottomSheetDialog(c);
        mDialogAddStatus.contentView(R.layout.dialog_select_me_user)
                .heightParam(ViewGroup.LayoutParams.MATCH_PARENT)
                .inDuration(500)
                .cancelable(true)
                .show();

        Window window = mDialogAddStatus.getWindow();
        window.setStatusBarColor(c.getResources().getColor(R.color.darkblue));
        RecyclerView rvChatlist = (RecyclerView) mDialogAddStatus.findViewById(R.id.rv_chatlist);
        sharedPreferredManager = new SharedPreferredManager(c);
        ImageView imClose = (ImageView) mDialogAddStatus.findViewById(R.id.im_close);
        TextView tvHeadingName = (TextView)  mDialogAddStatus.findViewById(R.id.tv_heading_name);
        tvHeadingName.setText("New Chat");
        tvHeadingName.setTextColor(c.getResources().getColor(R.color.white));
        EditText searchTv = (EditText)  mDialogAddStatus.findViewById(R.id.search_tv);

        LinearLayout llSearch = (LinearLayout)  mDialogAddStatus.findViewById(R.id.ll_search);
        llSearch.setVisibility(View.GONE);
        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogAddStatus.dismiss();
            }
        });
        ChatUsersListAdapter chatUsersListAdapter = new ChatUsersListAdapter(c, contactTbArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        rvChatlist.setLayoutManager(mLayoutManager);
        rvChatlist.setItemAnimator(new DefaultItemAnimator());
        rvChatlist.setAdapter(chatUsersListAdapter);
    }

    @Override
    public void onClickUserList(ContactTb contactTb, boolean isUserList) {
        mDialogAddStatus.dismiss();
        Intent intent = new Intent(c,ChatActivity.class);
        contactTb.setIsUsersList(true);
        intent.putExtra("contactTb",contactTb);
        c.startActivity(intent);
    }
}
