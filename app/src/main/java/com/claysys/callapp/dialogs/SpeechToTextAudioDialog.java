package com.claysys.callapp.dialogs;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.claysys.callapp.App;
import com.claysys.callapp.R;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.utlis.GrantingPermission;
import com.rey.material.app.BottomSheetDialog;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class SpeechToTextAudioDialog implements Runnable {

    private Context c;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private ImageView imStartRecord;
    private ImageView imStopRecord;
    private TextView speechTv;
    private CallLogTb callLogTb;
    private LinearLayout llAudioPlayerLayour;
    private LinearLayout llEditLayout;
    private ImageView play;
    private ImageView pause;
    private ProgressBar progressBar;
    private TextView starttime;
    private TextView endtime;
    LinearLayout btnEditAudioTxt;
    LinearLayout btnDeleteAudio;
    Button btnSaveAudioFile;
    EditText etSpeechText;

    MediaPlayer mediaPlayer;
    SeekBar seekBar;
    boolean wasPlaying = false;
    Uri mediaPlayerUri;

    int pauseFlag = 0;


    private static final int REQUEST_RECORD_PERMISSION = 100;
    OnCallSpeechToTextOnActivityResult onCallSpeechToTextOnActivityResult;


    public void openUpDialogToAdd(Context c, CallLogTb callLogTb, OnCallSpeechToTextOnActivityResult onCallSpeechToTextOnActivityResult) {
        this.c = c;
        this.callLogTb = callLogTb;
        this.onCallSpeechToTextOnActivityResult = onCallSpeechToTextOnActivityResult;
        final BottomSheetDialog mDialogAddStatus = new BottomSheetDialog(c);
        mDialogAddStatus.contentView(R.layout.record_dialog)
                .heightParam(ViewGroup.LayoutParams.WRAP_CONTENT)
                .inDuration(500)
                .cancelable(true)
                .show();
        mediaPlayer = new MediaPlayer();
        CallLogTb callLogTbFromDb = App.getInstance().getDB().scheduleDao().getMsg(callLogTb.getId());
        if (callLogTbFromDb.getMsg() != null)
            callLogTb.setMsg(callLogTbFromDb.getMsg());
        TextView tvTimeDuration = (TextView) mDialogAddStatus.findViewById(R.id.tv_time_duration);
        TextView tvPhoneNo = (TextView) mDialogAddStatus.findViewById(R.id.tv_ph_no);
        TextView tvName = (TextView) mDialogAddStatus.findViewById(R.id.tv_name);
        imStartRecord = (ImageView) mDialogAddStatus.findViewById(R.id.im_start_record);
        imStopRecord = (ImageView) mDialogAddStatus.findViewById(R.id.im_stop_record);
        speechTv = (TextView) mDialogAddStatus.findViewById(R.id.speech_tv);
        llAudioPlayerLayour = mDialogAddStatus.findViewById(R.id.audio_player_layour);
        play = mDialogAddStatus.findViewById(R.id.play_button);
        pause = mDialogAddStatus.findViewById(R.id.pause_button);
        starttime = mDialogAddStatus.findViewById(R.id.start_time_tv);
        etSpeechText = mDialogAddStatus.findViewById(R.id.et_speech_text);
        endtime = mDialogAddStatus.findViewById(R.id.end_time_tv);
        btnSaveAudioFile = mDialogAddStatus.findViewById(R.id.btn_save_audio_file);
        seekBar = mDialogAddStatus.findViewById(R.id.seekbar);
        llEditLayout = mDialogAddStatus.findViewById(R.id.edit_layout);
        btnEditAudioTxt = mDialogAddStatus.findViewById(R.id.btn_edit_audio_txt);
        btnDeleteAudio = mDialogAddStatus.findViewById(R.id.btn_delete_audio);
        ImageView imClose = (ImageView) mDialogAddStatus.findViewById(R.id.im_close);
        ImageView imgProfileImage = (ImageView) mDialogAddStatus.findViewById(R.id.img_profile_image);


        speechTv.setText(callLogTb.getMsg());
        speech = SpeechRecognizer.createSpeechRecognizer(c);
        if (callLogTbFromDb.getAudioFileUrl() != null) {
            llAudioPlayerLayour.setVisibility(View.VISIBLE);
            llEditLayout.setVisibility(View.VISIBLE);
            try {
                mediaPlayerUri = Uri.parse(callLogTbFromDb.getAudioFileUrl());
                mediaPlayer.setDataSource(c, Uri.parse(callLogTbFromDb.getAudioFileUrl()));
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            llAudioPlayerLayour.setVisibility(View.GONE);
            llEditLayout.setVisibility(View.GONE);
        }

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSong();
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseFlag =1;
                pause.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
                mediaPlayer.pause();
            }
        });
        btnSaveAudioFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSaveAudioFile.setTextColor(c.getResources().getColor(R.color.white));
                if (etSpeechText.getVisibility() == View.VISIBLE) {
                    speechTv.setVisibility(View.VISIBLE);
                    speechTv.setText(etSpeechText.getText().toString());
                    etSpeechText.setVisibility(View.GONE);
                }
                btnEditAudioTxt.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
                btnDeleteAudio.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
                btnSaveAudioFile.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_green));
                onCallSpeechToTextOnActivityResult.saveTheFileInList(speechTv.getText().toString());
            }
        });
        btnEditAudioTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSaveAudioFile.setTextColor(c.getResources().getColor(R.color.secondryTextColor));
                etSpeechText.setVisibility(View.VISIBLE);
                etSpeechText.setText(speechTv.getText().toString());
                speechTv.setVisibility(View.GONE);

                btnSaveAudioFile.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
                btnDeleteAudio.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
                btnEditAudioTxt.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_green));
            }
        });
        btnDeleteAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().getDB().scheduleDao().deleteCallLog(null, null, null, callLogTb.getId());
                llEditLayout.setVisibility(View.GONE);
                llAudioPlayerLayour.setVisibility(View.GONE);
                speechTv.setText("");
                callLogTb.setAmrFileUrl(null);
                callLogTb.setMsg(null);
                callLogTb.setAudioFileUrl(null);
                mDialogAddStatus.heightParam((int) c.getResources().getDimension(R.dimen._300sdp));


            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                starttime.setVisibility(View.VISIBLE);
//                int x = (int) Math.ceil(progress / 1000f);
//
//                if (x < 10)
//                    starttime.setText("0:0" + x);
//                else
//                    starttime.setText("0:" + x);

                long startTimePosition = mediaPlayer.getCurrentPosition();
                starttime.setText(String.format("%02d:%02d ",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTimePosition),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTimePosition) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                        startTimePosition)))
                );

                double percent = progress / (double) seekBar.getMax();
                int offset = seekBar.getThumbOffset();
                int seekWidth = seekBar.getWidth();
                int val = (int) Math.round(percent * (seekWidth - 2 * offset));
                int labelWidth = starttime.getWidth();
//                starttime.setX(offset + seekBar.getX() + val
//                        - Math.round(percent * offset)
//                        - Math.round(percent * labelWidth / 2));

                if (progress > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    play.setVisibility(View.VISIBLE);
                    pause.setVisibility(View.GONE);
                    if (progress == seekBar.getMax()) {
                        seekBar.setProgress(0);
                        starttime.setText("0:00");
                        clearMediaPlayer();
                    }
                    //         MainActivity.this.seekBar.setProgress(0);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }
        });

        Log.i("", "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(c));


        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                int duration = mediaPlayer.getDuration();
                String time = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(duration),
                        TimeUnit.MILLISECONDS.toSeconds(duration) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
                );
                endtime.setText(time);
            }
        });
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");

        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        recognizerIntent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        recognizerIntent.putExtra("android.speech.extra.GET_AUDIO", true);
        recognizerIntent.putExtra(RecognizerIntent.ACTION_RECOGNIZE_SPEECH, true);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 20000000);
//        recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS ,20000000);
//        recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS,30000);
//        Long timeStamp = Long.valueOf(callLogTb.getDate());
//        timeStamp = timeStamp*1000;
//
//
//        Date callDayTime = new Date(timeStamp);
//        SimpleDateFormat format = new SimpleDateFormat("hh:mm a, dd MMM yyyy ");
//        String date = format.format(callDayTime);
        tvTimeDuration.setText(callLogTb.getDateDiaplay());


        tvName.setText(callLogTb.getName());
        tvPhoneNo.setText(callLogTb.getPhNo());
        if (callLogTb.getImgeUrl() != null) {
            Glide.with(c)
                    .load(callLogTb.getImgeUrl())
                    .fitCenter()
                    .placeholder(c.getResources().getDrawable(R.drawable.img_avatar))
                    .into(imgProfileImage);
        }
        imStartRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imStartRecord.setVisibility(View.GONE);
                imStopRecord.setVisibility(View.VISIBLE);
                int permissionCallPhone12 = ContextCompat.checkSelfPermission(c, Manifest.permission.RECORD_AUDIO);
                if (permissionCallPhone12 != PackageManager.PERMISSION_GRANTED) {
                    new GrantingPermission().askForPermission((Activity) c, Manifest.permission.RECORD_AUDIO, REQUEST_RECORD_PERMISSION);
                    onCallSpeechToTextOnActivityResult.onCallBackToActivityResult(speechTv, imStartRecord, imStopRecord, llEditLayout, llAudioPlayerLayour, mDialogAddStatus);
                    ((Activity) c).startActivityForResult(recognizerIntent, 10);
                } else {
                    onCallSpeechToTextOnActivityResult.onCallBackToActivityResult(speechTv, imStartRecord, imStopRecord, llEditLayout, llAudioPlayerLayour, mDialogAddStatus);
                    ((Activity) c).startActivityForResult(recognizerIntent, 10);
                }

            }
        });
        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogAddStatus.dismiss();
            }
        });

        imStopRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imStartRecord.setVisibility(View.VISIBLE);
                imStopRecord.setVisibility(View.GONE);
//                speech.stopListening();
            }
        });
    }

    public void setMediaPlayerUri(Uri mediaPlayerUri) {
        if (mediaPlayer != null) {
            llAudioPlayerLayour.setVisibility(View.VISIBLE);
            llEditLayout.setVisibility(View.VISIBLE);
            btnEditAudioTxt.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
            btnDeleteAudio.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_lightgray));
            btnSaveAudioFile.setBackground(c.getResources().getDrawable(R.drawable.corner_radius_green));
            try {
                clearMediaPlayer();
                this.mediaPlayerUri = mediaPlayerUri;
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(c, mediaPlayerUri);
                mediaPlayer.prepare();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        int duration = mediaPlayer.getDuration();
                        String time = String.format("%02d:%02d ",
                                TimeUnit.MILLISECONDS.toMinutes(duration),
                                TimeUnit.MILLISECONDS.toSeconds(duration) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
                        );
                        endtime.setText(time);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            llAudioPlayerLayour.setVisibility(View.GONE);
            llEditLayout.setVisibility(View.GONE);
        }
    }


    public interface OnCallSpeechToTextOnActivityResult {
        void onCallBackToActivityResult(TextView tvSpeechToText, ImageView imButtonStart, ImageView imButtonStop, LinearLayout llEditLayout, LinearLayout llAudioPlayerLayour, BottomSheetDialog mDialogAddStatus);

        void saveTheFileInList(String speechToText);
    }


    public void playSong() {

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            clearMediaPlayer();
            seekBar.setProgress(0);
            wasPlaying = true;
            play.setVisibility(View.VISIBLE);
            pause.setVisibility(View.GONE);
        }
        if (!wasPlaying) {
            if(pauseFlag==0) {
                if(mediaPlayerUri!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(c, mediaPlayerUri);
                        mediaPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }else{
                pauseFlag = 0;
            }

            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            }

            play.setVisibility(View.GONE);
            pause.setVisibility(View.VISIBLE);


            mediaPlayer.setVolume(0.5f, 0.5f);
            mediaPlayer.setLooping(false);
            seekBar.setMax(mediaPlayer.getDuration());

            mediaPlayer.start();
            new Thread(this).start();

        }

        wasPlaying = false;

    }


    public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();


        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }

            seekBar.setProgress(currentPosition);

        }
    }


    private void clearMediaPlayer() {
        try {
            mediaPlayer.stop();
//            mediaPlayer.release();
        } catch (Exception e) {
        }
//        mediaPlayer = null;
    }


}
