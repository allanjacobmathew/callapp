package com.claysys.callapp.dialogs;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.App;
import com.claysys.callapp.ChatActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.adapter.ChatUsersListAdapter;
import com.claysys.callapp.adapter.SimpleContactListAdapter;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.rey.material.app.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SelectContactSiimpleDialog  {

    Context c;
    ArrayList<ContactTb> contactTbArrayList, globalContactList;
    SharedPreferredManager sharedPreferredManager;
    BottomSheetDialog mDialogAddStatus;
    SimpleContactListAdapter simpleContactListAdapter;

    public void openUpDialogToAdd(Context c,int position, ArrayList<ContactTb>  contactTbArrayList, SimpleContactListAdapter.OnClickContactToTagtUser onClickContactToTagtUser) {
        this.c =c;
        this.contactTbArrayList = contactTbArrayList;
        globalContactList =contactTbArrayList;
       mDialogAddStatus = new BottomSheetDialog(c);
        mDialogAddStatus.contentView(R.layout.dialog_select_me_user)
                .heightParam((int) c.getResources().getDimension(R.dimen._350sdp))
                .inDuration(500)
                .cancelable(true)
                .show();


        Window window = mDialogAddStatus.getWindow();
        window.setStatusBarColor(c.getResources().getColor(R.color.darkblue));
        RecyclerView rvChatlist = (RecyclerView) mDialogAddStatus.findViewById(R.id.rv_chatlist);
        sharedPreferredManager = new SharedPreferredManager(c);
        ImageView imClose = (ImageView) mDialogAddStatus.findViewById(R.id.im_close);
        TextView tvHeadingName = (TextView)  mDialogAddStatus.findViewById(R.id.tv_heading_name);
        LinearLayout llmainHeadingContainer = (LinearLayout)  mDialogAddStatus.findViewById(R.id.ll_main_heading_container);
        LinearLayout llHeadingContainer = (LinearLayout)  mDialogAddStatus.findViewById(R.id.ll_heading_container);
        EditText searchTv = (EditText)  mDialogAddStatus.findViewById(R.id.search_tv);

        simpleContactListAdapter = new SimpleContactListAdapter(c,position,mDialogAddStatus, contactTbArrayList,onClickContactToTagtUser);

        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogAddStatus.dismiss();
            }
        });
        searchTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                if (arg0.length() == 0) {
                    setFilterCallToAdapter(null);
                } else {
                    String text = searchTv.getText().toString().toLowerCase(Locale.getDefault());
                    setFilterCallToAdapter(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        llHeadingContainer.setBackgroundColor(c.getResources().getColor(R.color.white));
        llmainHeadingContainer.setBackgroundColor(c.getResources().getColor(R.color.white));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        rvChatlist.setLayoutManager(mLayoutManager);
        rvChatlist.setItemAnimator(new DefaultItemAnimator());
        rvChatlist.setAdapter(simpleContactListAdapter);
    }
    public void setFilterCallToAdapter(String text){
        if(text!=null) {
            simpleContactListAdapter.filter(text, contactTbArrayList);
        }else {
            if(globalContactList!=null) {
                simpleContactListAdapter.setAllDataList(globalContactList);
            }
        }

    }

}
