package com.claysys.callapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.MyTaskDetailActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.adapter.MyTasksAdapterList;
import com.claysys.callapp.models.MyTaskList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyTasksFragment extends Fragment implements MyTasksAdapterList.OnClickMyTaskListInterface {
    ArrayList<MyTaskList> myTaskDetailArrayList;
    @BindView(R.id.rv_my_task_list)
    RecyclerView rvMyTaskList;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_tasks, container, false);
        ButterKnife.bind(this, view);
        setAdapter();
        return view;

    }


    private void setAdapter() {
        myTaskDetailArrayList = new ArrayList<>();
        MyTaskList myTaskDetail1 = new MyTaskList();
        myTaskDetail1.setTaskDescription("Some description that to test the number of lines else to to dot dot dotdescription that to we want to test test description that to we want to test test description that to we want to test test  dot etc ");
        myTaskDetail1.setTaskName("Over Draft");
        myTaskDetail1.setTaskProgress("Hold");
        myTaskDetailArrayList.add(myTaskDetail1);
        MyTaskList myTaskDetail2 = new MyTaskList();
        myTaskDetail2.setTaskDescription("Some description that to test description that to we want to test test description that to we want to test test description that to we want to test test  the number of lines else to to dot dot dot dot etc ");
        myTaskDetail2.setTaskName("Credit Card");
        myTaskDetail2.setTaskProgress("Hold");
        myTaskDetailArrayList.add(myTaskDetail2);
        MyTaskList myTaskDetail3 = new MyTaskList();
        myTaskDetail3.setTaskDescription("Some description that to we want to test test what is to test we want to test want people what are they not test the number of lines else to to dot dot dot dot etc ");
        myTaskDetail3.setTaskName("Auto Loan");
        myTaskDetail3.setTaskProgress("Hold");
        myTaskDetailArrayList.add(myTaskDetail3);
        MyTasksAdapterList myTasksAdapterList = new MyTasksAdapterList(getActivity(), myTaskDetailArrayList, this::onClickMytaskList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvMyTaskList.setLayoutManager(mLayoutManager);
        rvMyTaskList.setItemAnimator(new DefaultItemAnimator());
        rvMyTaskList.setAdapter(myTasksAdapterList);

    }


    @Override
    public void onClickMytaskList(MyTaskList MyTaskList) {
        Intent intent = new Intent(getActivity(), MyTaskDetailActivity.class);
        startActivity(intent);
    }
}
