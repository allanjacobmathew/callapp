package com.claysys.callapp.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.App;
import com.claysys.callapp.ChatActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.adapter.ContactAdapterList;
import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.helper.PhoneCallHelper;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.SaveCallLogReq;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.AppConstants;
import com.claysys.callapp.utlis.GrantingPermission;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactFragment extends Fragment implements ContactAdapterList.OnClickContactAdapter, IResponseView {

    @BindView(R.id.rv_contactlist)
    RecyclerView rvContactlist;
    SaveCallLogReq saveCallLogReq;
    ArrayList<ContactTb> contactTbsWithoutFilter;
    ContactAdapterList contactAdapterList;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_screen, container, false);
        ButterKnife.bind(this, view);

        setLiveData();
        int permissionCallPhone15 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (permissionCallPhone15 != PackageManager.PERMISSION_GRANTED) {
            new GrantingPermission().askForPermission(getActivity(), Manifest.permission.CALL_PHONE, AppConstants.CALL);
        }
        return view;

    }

    private void setLiveData() {
        LiveData<List<ContactTb>> contacttbArrayList =  App.getInstance().getDB().scheduleDao().getAllContactTb();

        contacttbArrayList.observe(this, new Observer<List<ContactTb>>() {
            @Override
            public void onChanged(List<ContactTb> contact_tbs) {
                ArrayList<ContactTb> contact_tbArrayList = (ArrayList<ContactTb>) contact_tbs;
                setAdapter(contact_tbArrayList);
            }
        });
    }

    private void setAdapter(ArrayList<ContactTb> contact_tbArrayList) {
        contactTbsWithoutFilter = contact_tbArrayList;

        contactAdapterList = new ContactAdapterList(getActivity(), contact_tbArrayList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvContactlist.setLayoutManager(mLayoutManager);
        rvContactlist.setItemAnimator(new DefaultItemAnimator());
        rvContactlist.setAdapter(contactAdapterList);

    }
    public void setFilterCallToAdapter(String text){
        if(text!=null) {
            contactAdapterList.filter(text, contactTbsWithoutFilter);
        }else {
            if(contactTbsWithoutFilter!=null) {
                contactAdapterList.setAllDataList(contactTbsWithoutFilter);
            }
        }

    }


    public static boolean isSimSupport(Context context)
    {
        boolean isAvailable = false;
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT: //SimState = “No Sim Found!”;
                isAvailable = false;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = “Network Locked!”;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = “PIN Required to access SIM!”;
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = “PUK Required to access SIM!”; // Personal Unblocking Code
                break;
            case TelephonyManager.SIM_STATE_READY:
                isAvailable = true;
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = “Unknown SIM State!”;
                break;
        }
        return isAvailable;
    }

    public void makeAPhoneCall(SaveCallLogReq saveCallLogReq){
        showDialogForCall(saveCallLogReq);
    }

    private void showDialogForCall(SaveCallLogReq saveCallLogReq) {
        int flag =0;
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_ok);
        dialog.getWindow().setLayout((int) getResources().getDimension(R.dimen._300sdp), (int) getResources().getDimension(R.dimen._120sdp));
        dialog.setCanceledOnTouchOutside(false);

        TextView okGotIt = dialog.findViewById(R.id.cancel);
        TextView cancel = dialog.findViewById(R.id.ok);
        cancel.setVisibility(View.VISIBLE);
        okGotIt.setText("Call");
        TextView errorTv = dialog.findViewById(R.id.error_tv);
        if (saveCallLogReq.getNumber() != null) {
            errorTv.setText(saveCallLogReq.getNumber());
        }

        okGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dialog.dismiss();
               Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + saveCallLogReq.getNumber()));

                startActivity(intent);
                Calendar calendar = Calendar.getInstance();
                System.out.println("Calender - Time in milliseconds : " + calendar.getTimeInMillis());
                saveCallLogReq.setTime(calendar.getTimeInMillis()/1000);
                saveCallLog(saveCallLogReq);

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void saveCallLog(SaveCallLogReq saveCallLogReq) {
        ApiHelper.getInstance().saveCallLogApi(getActivity(), this,saveCallLogReq);
    }


    @Override
    public void onClickContact(ContactTb contactTb, boolean isWork) {
         saveCallLogReq = new SaveCallLogReq();
        saveCallLogReq.setContactId(Integer.parseInt(contactTb.getId()));
        if(isWork) {
            saveCallLogReq.setLogType(AppConstants.Work);
            saveCallLogReq.setNumber(contactTb.getAlternate());
        }else {
            saveCallLogReq.setLogType(AppConstants.Mobile);
            saveCallLogReq.setNumber(contactTb.getMobile());
        }
        saveCallLogReq.setEmailID(contactTb.getEmailID());

        saveCallLogReq.setGuid(contactTb.getGuid());


        if (!isSimSupport(getActivity())){
            new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Alert")
                    .setMessage("Simcard is not supported. Do you want to continue? ")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int permissionCallPhone = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);
                            if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
                                new GrantingPermission().askForPermission(getActivity(), Manifest.permission.CALL_PHONE, AppConstants.CALL);
                            } else {
//                                if(isWork)
//                                    makeAPhoneCall(saveCallLogReq);
//                                else
                                    makeAPhoneCall(saveCallLogReq);
                            }
                        }
                    }).setNegativeButton("No", null).show();
        }else{
            int permissionCallPhone = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);
            if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
                new GrantingPermission().askForPermission(getActivity(), Manifest.permission.CALL_PHONE, AppConstants.CALL);
            } else {
//                if(isWork)
//                    makeAPhoneCall(saveCallLogReq);
//                else
                    makeAPhoneCall(saveCallLogReq);
            }
        }


    }

    @Override
    public void onGoToMsgActivity(ContactTb contactTb) {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        getContext().startActivity(intent);

    }

    @Override
    public void onSaveCallLogsSucess(SaveCallLogResp saveCallLogResp) {
        if(saveCallLogResp.getStatus().equals(AppConstants.Success)) {
            CallLogTb callLogTb = new CallLogTb();
            callLogTb.setLogId(saveCallLogResp.getId());
            callLogTb.setUserid(String.valueOf(saveCallLogReq.getContactId()));
            callLogTb.setLog(saveCallLogReq.getLogType());
            callLogTb.setPhNo(saveCallLogReq.getNumber());
            callLogTb.setNumberType(saveCallLogReq.getLogType());
            callLogTb.setDate(String.valueOf(saveCallLogReq.getTime()));
            long id = App.getInstance().getDB().scheduleDao().insertCalLLogDetails(callLogTb);
        }
    }

    @Override
    public void onSaveCallLogsFailure() {

    }
}
