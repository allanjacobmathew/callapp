package com.claysys.callapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Movie;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.App;
import com.claysys.callapp.MainActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.RecordActivity;
import com.claysys.callapp.adapter.CallLogAdapterList;
import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.dialogs.SpeechToTextAudioDialog;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.SaveCallLogNoteReq;
import com.claysys.callapp.sqllite.helper.CallLogTb;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.utlis.GrantingPermission;
import com.rey.material.app.BottomSheetDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.SequenceInputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallLogFragment extends Fragment implements IResponseView,CallLogAdapterList.OnClickCallLogAdapter, SpeechToTextAudioDialog.OnCallSpeechToTextOnActivityResult {


    @BindView(R.id.rv_loglist)
    RecyclerView rvLogList;

    TextView speechToTextTv;
    ImageView imButtonStart;
    ImageView imButtonStop;
    LinearLayout llEditLayout;
    LinearLayout llAudioPlayerLayour;
    CallLogTb callLogTb;
    File audioFile;
    SpeechToTextAudioDialog speechToTextAudioDialog;
    BottomSheetDialog mDialogAddStatus;
    private static final int REQUEST_RECORD_PERMISSION = 100;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_log, container, false);
        ButterKnife.bind(this, view);
        getCallLogDetails();
        int permissionCallPhone12 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        if (permissionCallPhone12 != PackageManager.PERMISSION_GRANTED) {
            new GrantingPermission().askForPermission( getActivity(), Manifest.permission.RECORD_AUDIO, REQUEST_RECORD_PERMISSION);
        }


        return view;
    }

    private void getCallLogDetails() {

        LiveData<List<CallLogTb>> allCallLogs = App.getInstance().getDB().scheduleDao().getAllCallLogs();

        allCallLogs.observe(this, new Observer<List<CallLogTb>>() {
            @Override
            public void onChanged(List<CallLogTb> contact_tbs) {
                ArrayList<CallLogTb> callLogTbArrayList = (ArrayList<CallLogTb>) contact_tbs;
                setCallLogAdapter(callLogTbArrayList);
            }
        });
    }

    private void setCallLogAdapter(ArrayList<CallLogTb> callLogTbArrayList) {
        if (!callLogTbArrayList.isEmpty()) {
            for (int i = 0; i < callLogTbArrayList.size(); i++) {
                ContactTb contactTb = App.getInstance().getDB().scheduleDao().getContactById(Integer.parseInt(callLogTbArrayList.get(i).getUserid()));
                callLogTbArrayList.get(i).setName(contactTb.getName()+" "+contactTb.getLastName());
                callLogTbArrayList.get(i).setImgeUrl(contactTb.getImageUrl());
            }
            CallLogAdapterList callLogAdapterList = new CallLogAdapterList(getActivity(), callLogTbArrayList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            rvLogList.setLayoutManager(mLayoutManager);
            rvLogList.setItemAnimator(new DefaultItemAnimator());
            rvLogList.setAdapter(callLogAdapterList);
        }
    }






    @Override
    public void onClickCallLog(CallLogTb callLogTb) {
//        Intent intent = new Intent(getActivity(), RecordActivity.class);
//        String callLogId = String.valueOf(callLogTb.getId());
//        intent.putExtra("callLogTb",callLogTb);
////        intent.putExtra("callLogId", callLogId);
////        intent.putExtra("name", callLogTb.getName());
////        intent.putExtra("phNo", callLogTb.getPhNo());
//        startActivity(intent);
        this.callLogTb = callLogTb;
        ((MainActivity)getActivity()).setCallogLogList(callLogTb);
        speechToTextAudioDialog = new SpeechToTextAudioDialog();
        speechToTextAudioDialog.openUpDialogToAdd(getActivity(),callLogTb,this);
    }

    public  void setSpeechToTextView(String speechToText){
        App.getInstance().getDB().scheduleDao().update_msg(callLogTb.getMsg()+" "+speechToText, String.valueOf(callLogTb.getId()));
        if(callLogTb.getMsg()!=null)
            speechToTextTv.setText(callLogTb.getMsg()+" "+speechToText);
        else
            speechToTextTv.setText(speechToText);
        imButtonStart.setVisibility(View.VISIBLE);
        imButtonStop.setVisibility(View.GONE);
    }


    @Override
    public void onCallBackToActivityResult(TextView tvSpeechToText, ImageView imButtonStart, ImageView imButtonStop, LinearLayout llEditLayout, LinearLayout llAudioPlayerLayour, BottomSheetDialog mDialogAddStatus) {
            this.speechToTextTv = tvSpeechToText;
            this.imButtonStart = imButtonStart;
            this.imButtonStop = imButtonStop;
            this.llEditLayout =llEditLayout;
            this.llAudioPlayerLayour =llAudioPlayerLayour;
            this.mDialogAddStatus =mDialogAddStatus;
    }

    @Override
    public void saveTheFileInList(String speechToText) {
        callLogTb.setMsg(speechToText);
        App.getInstance().getDB().scheduleDao().update_msg(speechToText, String.valueOf(callLogTb.getId()));
        ApiHelper.getInstance().saveAudioFilesApi(getActivity(), this, audioFile);

    }

    public void setSpeechToMediaPlayer(File file, Uri savedImageURI,boolean sucess) {
        if(sucess) {
            audioFile = file;
            speechToTextAudioDialog.setMediaPlayerUri(savedImageURI);
            llAudioPlayerLayour.setVisibility(View.VISIBLE);
            llEditLayout.setVisibility(View.VISIBLE);
            mDialogAddStatus.heightParam((int) getActivity().getResources().getDimension(R.dimen._450sdp));

        }else{

            imButtonStart.setVisibility(View.VISIBLE);
            imButtonStop.setVisibility(View.GONE);
            if(callLogTb.getMsg()==null) {
                llAudioPlayerLayour.setVisibility(View.GONE);
                llEditLayout.setVisibility(View.GONE);
                mDialogAddStatus.heightParam(LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        }
    }



    @Override
    public void onSaveCallLogsAudioFileSucess(String status, String fileUrl) {

        SaveCallLogNoteReq saveCallLogNoteReq = new SaveCallLogNoteReq();
        saveCallLogNoteReq.setFileUrl(fileUrl);
        saveCallLogNoteReq.setNotes(callLogTb.getMsg());
        saveCallLogNoteReq.setLogId(callLogTb.getLogId());
        ApiHelper.getInstance().saveCallLogNoteApi(getActivity(), this, saveCallLogNoteReq);
    }

}
