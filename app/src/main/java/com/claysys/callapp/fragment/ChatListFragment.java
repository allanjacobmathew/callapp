package com.claysys.callapp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.claysys.callapp.App;
import com.claysys.callapp.ChatActivity;
import com.claysys.callapp.R;
import com.claysys.callapp.adapter.ChatUsersListAdapter;
import com.claysys.callapp.apicontroller.ApiHelper;
import com.claysys.callapp.dialogs.SelectMeUserDialog;
import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.rest.interfaceviews.IResponseView;
import com.claysys.callapp.rest.request.LoginApiReq;
import com.claysys.callapp.rest.response.LoginResp;
import com.claysys.callapp.rest.response.SaveCallLogResp;
import com.claysys.callapp.sqllite.helper.ContactTb;
import com.claysys.callapp.sqllite.helper.EmployeeTb;
import com.claysys.callapp.utlis.AppConstants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.rey.material.app.BottomSheetDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ChatListFragment extends Fragment implements IResponseView, ChatUsersListAdapter.OnClickChatListUser {


    @BindView(R.id.rv_chatlist)
    RecyclerView rvChatlist;
    ArrayList<ContactTb> contactTbsUsers;
    ArrayList<ContactTb> contactTbsChatList;
    FirebaseFirestore db;
    SharedPreferredManager sharedPreferredManager;

    Boolean isNumberExist = false;
    ContactTb contactTbReg;

    int i = 0;
    BottomSheetDialog mDialogAddStatus;
    @BindView(R.id.tv_no_chats)
    TextView tvNoChats;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        ButterKnife.bind(this, view);
        db = FirebaseFirestore.getInstance();
        sharedPreferredManager = new SharedPreferredManager(getContext());
        callChatUsersListFirebase();

        contactTbsChatList = new ArrayList<>();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    public void setUsersListAdapter() {

        contactTbsUsers = new ArrayList<>();
        if (sharedPreferredManager.getUserFirebaseId()!=null) {
            db.collection("users")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {

                                    if (!sharedPreferredManager.getUserFirebaseId().equals(document.getId())) {
                                        Log.d("", document.getId() + " => " + document.getData());
                                        String iddoc = document.getId();
                                        ContactTb contactTb = new ContactTb();
                                        contactTb.setDocment_id(document.getId());
                                        contactTb.setIsUsersList(true);
                                        contactTb.setName(String.valueOf(document.getData().get("displayName")));
                                        contactTb.setMobile(String.valueOf(document.getData().get("mobile")));
                                        contactTbsUsers.add(contactTb);
                                    }

                                }
                                new SelectMeUserDialog().openUpDialogToAdd(getActivity(), contactTbsUsers);
                            } else {

                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }
        Log.e("", "");
    }


    private void setAdapterData(ArrayList<ContactTb> contactTbs) {
        if(contactTbs.isEmpty()){
            tvNoChats.setVisibility(View.VISIBLE);
        }else {
            tvNoChats.setVisibility(View.GONE);
            ChatUsersListAdapter chatUsersListAdapter = new ChatUsersListAdapter(getActivity(), contactTbs, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            rvChatlist.setLayoutManager(mLayoutManager);
            rvChatlist.setItemAnimator(new DefaultItemAnimator());
            rvChatlist.setAdapter(chatUsersListAdapter);
        }

    }

    @Override
    public void onClickUserList(ContactTb contactTb, boolean isUserList) {
        Intent intent = new Intent(getContext(), ChatActivity.class);
        contactTb.setIsUsersList(false);
        intent.putExtra("contactTb", contactTb);
        startActivity(intent);

    }

    private void setEmployeeApi(ContactTb contactTbReg) {
        this.contactTbReg = contactTbReg;
        ApiHelper.getInstance().addEmploye(getActivity(), this, contactTbReg);
    }

    @Override
    public void onSaveCallLogsSucess(SaveCallLogResp saveCallLogResp) {
        if (saveCallLogResp.getStatus().equals(AppConstants.Success)) {
            EmployeeTb employeeTb = new EmployeeTb();
            employeeTb.setId(saveCallLogResp.getId());
            employeeTb.setFirstName(contactTbReg.getFirstName());
            employeeTb.setLastName(contactTbReg.getLastName());
            employeeTb.setEmailID(contactTbReg.getEmailID());
            employeeTb.setFirebaseId(contactTbReg.getDocment_id());
            employeeTb.setMobile(contactTbReg.getMobile());
            App.getInstance().getDB().scheduleDao().insertEmployeeDetails(employeeTb);
        }

    }

    public void callChatUsersListFirebase() {
        db.collection(AppConstants.channel).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "listen:error", e);
                    return;
                }

                for (DocumentChange dc : snapshots.getDocumentChanges()) {
                    switch (dc.getType()) {
                        case ADDED:
                            Log.d(TAG, "New city: " + dc.getDocument().getData());
                            callDataUpdateFnctn(dc.getDocument());
                            break;
                        case MODIFIED:
                            updateContactChatUserList(dc.getDocument());
                            Log.d(TAG, "Modified city: " + dc.getDocument().getData());
                            break;
                        case REMOVED:
                            Log.d(TAG, "Removed city: " + dc.getDocument().getData());
                            break;
                    }
                }
            }
        });
    }

    private void updateContactChatUserList(QueryDocumentSnapshot document) {
        if (contactTbsChatList != null) {
            boolean containsData = filter(document.getId(), contactTbsChatList);
            if (containsData) {
                contactTbsChatList.get(i).setLast_msg(String.valueOf(document.getData().get("lastMessageContent")));
                try {
                    Timestamp timestamp = (Timestamp) document.getData().get("lastUpdateDate");
                    if (timestamp != null) {
                        contactTbsChatList.get(i).setMsg_timestamp(String.valueOf(timestamp.getSeconds()));
                    }
                } catch (Exception e) {
                }
            } else {
                callDataUpdateFnctn(document);
            }
            setAdapterData(contactTbsChatList);
        }
    }

    private void callDataUpdateFnctn(QueryDocumentSnapshot document) {

        Log.d("", document.getId() + " => " + document.getData());
        if (String.valueOf(document.getData().get("userId1")).equals(sharedPreferredManager.getUserFirebaseId()) || String.valueOf(document.getData().get("userId2")).equals(sharedPreferredManager.getUserFirebaseId())) {
            String iddoc = document.getId();
            ContactTb contactTb = new ContactTb();
            contactTb.setIsUsersList(false);
            contactTb.setDocment_id(document.getId());
            contactTb.setMemberid1(String.valueOf(document.getData().get("userId1")));
            contactTb.setMemberid2(String.valueOf(document.getData().get("userId2")));
            contactTb.setLast_msg(String.valueOf(document.getData().get("lastMessageContent")));
            try {
                Timestamp timestamp = (Timestamp) document.getData().get("lastUpdateDate");
                if (timestamp != null) {
                    contactTb.setMsg_timestamp(String.valueOf(timestamp.getSeconds()));
                }
            } catch (Exception e) {
                String timeStamp = document.getData().get("lastUpdateDate").toString();
                if(timeStamp!="") {
                    timeStamp = timeStamp.replace(".", "");
                    String date = timeStamp.substring(0, timeStamp.indexOf("E"));
                    contactTb.setMsg_timestamp(date);
                }
            }

            String docmentpath;
            String msg;
            if (contactTb.getMemberid1().equals(sharedPreferredManager.getUserFirebaseId())) {
                docmentpath = contactTb.getMemberid2();
            } else {
                docmentpath = contactTb.getMemberid1();
            }
            DocumentReference docRef = db.collection("users").document(docmentpath);
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            ContactTb userdtails = new ContactTb();
                            userdtails.setIsUsersList(false);
                            userdtails.setName(String.valueOf(document.getData().get("displayName")));
                            userdtails.setMobile(String.valueOf(document.getData().get("mobile")));
                            contactTb.setName(String.valueOf(document.getData().get("displayName")));
                            contactTb.setUserDetails(userdtails);

                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                    boolean containsData = filter(contactTb.getDocment_id(), contactTbsChatList);
                    if (containsData) {
                        int k = i;
                        if (contactTb.getLast_msg() != null) {
                            contactTbsChatList.get(k).setLast_msg(contactTb.getLast_msg());
                        }
                        if (contactTb.getMsg_timestamp() != null) {
                            contactTbsChatList.get(k).setMsg_timestamp(contactTb.getMsg_timestamp());
                        }
                    } else {
                        contactTbsChatList.add(contactTb);
                    }

                    setAdapterData(contactTbsChatList);
                }

            });


        }
        setAdapterData(contactTbsChatList);

    }

    public boolean filter(String id, ArrayList<ContactTb> contactTbArrayList) {
        //new array list that will hold the filtered data
        ArrayList<ContactTb> filterdNames = new ArrayList<>();
        i = 0;
        //looping through existing elements
        for (ContactTb s : contactTbArrayList) {

            //if the existing elements contains the search input
            if (s.getDocment_id().contains(id)) {
                ContactTb contactDetails = new ContactTb();
                contactDetails = s;
                //adding the element to filtered list
                filterdNames.add(contactDetails);
                break;
            }
            i++;
        }
        if (filterdNames.size() > 0) {
            return true;
        } else {
            return false;
        }
    }


}

