package com.claysys.callapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.claysys.callapp.helper.SharedPreferredManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreenActivity extends AppCompatActivity {
    SharedPreferredManager sharedPreferredManager;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sharedPreferredManager = new SharedPreferredManager(this);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.mrmBackground));
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("aaaa", "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("aaaa", "signInAnonymously:failure", task.getException());
                            Toast.makeText(SplashScreenActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sharedPreferredManager.IsUser()){
                    Intent intent  = new Intent(SplashScreenActivity.this , MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent  = new Intent(SplashScreenActivity.this , LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        }, 1000 * 3);


    }
}
