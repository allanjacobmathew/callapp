package com.claysys.callapp.utlis;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by muhammed on 9/27/2017.
 */

public class AppUtils {
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }
}
