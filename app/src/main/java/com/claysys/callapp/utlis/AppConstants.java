package com.claysys.callapp.utlis;

public class AppConstants {

    //PERMISSIONS
    public static final Integer CALL = 0x2;
    public static final Integer READPHONE = 0x8;
    public static final Integer ACCOUNTS = 0x6;
    public static final Integer READ_CONTACTS = 0x4;
    public static final Integer CAMERA = 0x5;
    public static final Integer READ_STORAGE = 0x3;
    public static final Integer WRITE_CALL_LOG = 0x9;
    public static final Integer READ_CALL_LOG = 0x1;
    public static final Integer WRITE_EXTERNAL_STORAGE = 0x6;
    public static final Integer RECORD_AUDIO = 0x7;

//    public static final String BASE_URL = "http://192.168.20.109:8500/api/";
    public static final String BASE_URL = "http://209.190.47.218:90/api/";
    public static final int GETALLCONTACTS = 1 ;
    public static int SAVEAUDIOFILES = 3;

    //shared
    public static String AccessToken = "accessToken";
    public static String RefreshedToken = "refreshedToken";
    public static String HAS_REFRESH_TOKEN = "hasRefreshedToken";
    public static String HAS_TOKEN = "hasToken";
    public static String DeviceIdApi = "deviceIdApi";
    public static String UserLocalId = "userLocalId";
    public static String UserMainId = "userMainId";
    public static String GUUID = "guid";
    public static String Username = "username";
    public static String deviceToken = "deviceToken";
    public static String EmailId = "emailId";

    public static String recents = "Recents";
    public static String contacts = "Contacts";
    public static String SetCallLogs ="GetCallLogsDetails";
    public static String Phone ="phone";
    public static String Success ="Success";

    public static String IsUser = "userAdded";

    public static String UserFirebaseId = "userFirebaseId";

    public static String ChatLists ="ChatLists";
    public static String MYTASKS ="My Tasks";
    public static String TagAMember ="Tag a Member";
//    public static String channel ="ChannelAndroid";
    public static String channel ="channels";
    public static String Mobile ="Mobile";
    public static String Work ="Work";
    public static String Bearer ="Bearer ";
}