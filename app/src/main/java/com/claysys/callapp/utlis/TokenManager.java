package com.claysys.callapp.utlis;

import android.content.Context;

import com.claysys.callapp.helper.SharedPreferredManager;

/**
 * Created by muhammed on 9/27/2017.
 */

public class TokenManager {
    private static TokenManager mTokenManager;
    private Context context;
    private String mToken;
    private boolean hasToken = false;
    private boolean hasRefreshToken = false;
    private String deviceId;
    private String refreshToken;
    SharedPreferredManager sharedPreferredManager;

    private TokenManager(Context context) {
        this.context = context;
        sharedPreferredManager = new SharedPreferredManager(context);
    }

    public static TokenManager getInstance(Context context) {

        return (mTokenManager == null) ? (mTokenManager = new TokenManager(context)) : mTokenManager;
    }

    public String getToken() {
        return mToken = sharedPreferredManager.getAccessToken();
    }

    public void setToken(String accessToken) {
        hasToken = true;
        sharedPreferredManager.setAccessToken( accessToken);
        sharedPreferredManager.insertTokenStatus( true);
    }

    public boolean hasToken() {
        return sharedPreferredManager.hasToken();
    }

    public void clearToken() {
        mToken = "";
        hasToken = false;
    }
//
    public String refreshToken() {
        return mToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
//
    public String getRefreshToken() {
        return refreshToken = sharedPreferredManager.getRefreshToken();
    }
//
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        this.hasRefreshToken = true;
        sharedPreferredManager.insertRefreshToken( refreshToken);
        sharedPreferredManager.insertRefreshTokenStatus( true);
    }


    public boolean hasRefreshToken() {
        return sharedPreferredManager.hasRefreshToken();
    }
}
