package com.claysys.callapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.claysys.callapp.sqllite.helper.ContactTb;


public class GroupChatMsgs implements Parcelable {
    public int getGroupsDetailId() {
        return groupsDetailId;
    }

    public void setGroupsDetailId(int groupsDetailId) {
        this.groupsDetailId = groupsDetailId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgSenderId() {
        return msgSenderId;
    }

    public void setMsgSenderId(String msgSenderId) {
        this.msgSenderId = msgSenderId;
    }

    public int getChatTypeId() {
        return chatTypeId;
    }

    public void setChatTypeId(int chatTypeId) {
        this.chatTypeId = chatTypeId;
    }

    public String getCreatedDateTimeStamp() {
        return createdDateTimeStamp;
    }

    public void setCreatedDateTimeStamp(String createdDateTimeStamp) {
        this.createdDateTimeStamp = createdDateTimeStamp;
    }

    public double getDeliveryDateTimeStamp() {
        return DeliveryDateTimeStamp;
    }

    public void setDeliveryDateTimeStamp(double deliveryDateTimeStamp) {
        DeliveryDateTimeStamp = deliveryDateTimeStamp;
    }

    public ContactTb getSenderId() {
        return senderId;
    }

    public void setSenderId(ContactTb senderId) {
        this.senderId = senderId;
    }

    public ContactTb getSender() {
        return sender;
    }

    public void setSender(ContactTb sender) {
        this.sender = sender;
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    int groupsDetailId;
    int id;

    public String getMsgReceiverId() {
        return msgReceiverId;
    }

    public void setMsgReceiverId(String msgReceiverId) {
        this.msgReceiverId = msgReceiverId;
    }

    String msg;
    String msgSenderId;

    public String getStartMsgSerevrId() {
        return startMsgSerevrId;
    }

    public void setStartMsgSerevrId(String startMsgSerevrId) {
        this.startMsgSerevrId = startMsgSerevrId;
    }

    String startMsgSerevrId;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    String channelId;

    String msgReceiverId;

    int chatTypeId;

    String createdDateTimeStamp;

    double DeliveryDateTimeStamp;

    @Ignore
    ContactTb senderId;
    @Ignore
    ContactTb sender;

    boolean isEnd;

    public String getTaggedUserId() {
        return taggedUserId;
    }

    public void setTaggedUserId(String taggedUserId) {
        this.taggedUserId = taggedUserId;
    }

    String taggedUserId;

    public String getUserToBeTagged() {
        return userToBeTagged;
    }

    public void setUserToBeTagged(String userToBeTagged) {
        this.userToBeTagged = userToBeTagged;
    }

    String userToBeTagged;
    String msgDocumentId;

    public String getMsgDocumentId() {
        return msgDocumentId;
    }

    public void setMsgDocumentId(String msgDocumentId) {
        this.msgDocumentId = msgDocumentId;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    boolean isStart;


    public boolean isSenderUser() {
        return isSenderUser;
    }

    public void setSenderUser(boolean senderUser) {
        isSenderUser = senderUser;
    }

    public String getTaggedUserName() {
        return taggedUserName;
    }

    public void setTaggedUserName(String taggedUserName) {
        this.taggedUserName = taggedUserName;
    }

    @Ignore
    boolean isSenderUser;


    String taggedUserName;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.groupsDetailId);
        dest.writeInt(this.id);
        dest.writeString(this.msg);
        dest.writeString(this.msgSenderId);
        dest.writeString(this.startMsgSerevrId);
        dest.writeString(this.channelId);
        dest.writeString(this.msgReceiverId);
        dest.writeInt(this.chatTypeId);
        dest.writeString(this.createdDateTimeStamp);
        dest.writeDouble(this.DeliveryDateTimeStamp);
        dest.writeParcelable(this.senderId, flags);
        dest.writeParcelable(this.sender, flags);
        dest.writeByte(this.isEnd ? (byte) 1 : (byte) 0);
        dest.writeString(this.taggedUserId);
        dest.writeString(this.msgDocumentId);
        dest.writeByte(this.isStart ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isSenderUser ? (byte) 1 : (byte) 0);
        dest.writeString(this.taggedUserName);
    }

    public GroupChatMsgs() {
    }

    protected GroupChatMsgs(Parcel in) {
        this.groupsDetailId = in.readInt();
        this.id = in.readInt();
        this.msg = in.readString();
        this.msgSenderId = in.readString();
        this.startMsgSerevrId = in.readString();
        this.channelId = in.readString();
        this.msgReceiverId = in.readString();
        this.chatTypeId = in.readInt();
        this.createdDateTimeStamp = in.readString();
        this.DeliveryDateTimeStamp = in.readDouble();
        this.senderId = in.readParcelable(ContactTb.class.getClassLoader());
        this.sender = in.readParcelable(ContactTb.class.getClassLoader());
        this.isEnd = in.readByte() != 0;
        this.taggedUserId = in.readString();
        this.msgDocumentId = in.readString();
        this.isStart = in.readByte() != 0;
        this.isSenderUser = in.readByte() != 0;
        this.taggedUserName = in.readString();
    }

    public static final Parcelable.Creator<GroupChatMsgs> CREATOR = new Parcelable.Creator<GroupChatMsgs>() {
        @Override
        public GroupChatMsgs createFromParcel(Parcel source) {
            return new GroupChatMsgs(source);
        }

        @Override
        public GroupChatMsgs[] newArray(int size) {
            return new GroupChatMsgs[size];
        }
    };
}
