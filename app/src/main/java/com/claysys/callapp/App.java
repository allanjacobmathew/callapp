package com.claysys.callapp;

import android.app.Application;

import android.content.Context;
import android.util.Log;


import androidx.room.Room;

import com.claysys.callapp.helper.SharedPreferredManager;
import com.claysys.callapp.rest.service.OkClientFactory;
import com.claysys.callapp.sqllite.base.AppDatabase;

import com.claysys.callapp.utlis.AppUtils;
import com.claysys.callapp.utlis.TokenManager;
import com.facebook.stetho.Stetho;

import okhttp3.OkHttpClient;


public class App extends Application {
    private static App mInstance;
    private static Context context;
    private AppDatabase database;

    private static final String DATABASE_NAME = "CallLogDb";

    public static App getInstance() {
        return mInstance;
    }


    private OkHttpClient mOkHttpClient;


    public static Context getAppContext() {
        return App.context;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }


    public AppDatabase getDB() {
        return database;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        App.context = getApplicationContext();

        mOkHttpClient = OkClientFactory.provideOkHttpClient(this);
        mInstance = this;
        new SharedPreferredManager(context).insertDeviceIdApi( AppUtils.getDeviceId(getAppContext()));
        TokenManager.getInstance(this).setDeviceId(AppUtils.getDeviceId(getAppContext()));
        String token = TokenManager.getInstance(getAppContext()).getToken();
        Log.d("DEVICE_ID", "onCreate: " + token);


        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
        Stetho.initializeWithDefaults(this);
    }


}
